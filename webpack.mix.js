let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*
 Share bet CSS Libraries
 Copy the img folder to suppress error
 */
//mix.copy('public/img', 'resources/assets/css/img');
mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
;

mix.scripts([
    'public/fe/js/jquery-2.1.1.js',
    'public/fe/js/main.js',
    'public/fe/js/modernizr.js',
], 'public/js/frontend.js')
    .styles([
        'public/fe/lib/gridiculous.css',
        'public/fe/lib/style.css',
        'public/fe/lib/menu.css',
    ], 'public/css/frontend.css')
;

mix.browserSync('sharebet.local');