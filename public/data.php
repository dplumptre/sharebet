<?php

$generate_betcodes = function($count){
    $start = $count;
    for ( $i=0; $i<=$start; $i++ )
    {
        echo random_int(1111111111, 9999999999) . "<br>";
    }
};
$teams = [
    'AFC Bournemouth',
    'Arsenal',
    'Brighton & Hove Albion',
    'Burnley',
    'Chelsea',
    'Crystal Palace',
    'Everton',
    'Huddersfield Town',
    'Leicester City',
    'Liverpool',
    'Manchester City',
    'Manchester United',
    'Newcastle United',
    'Southampton',
    'Stoke City',
    'Swansea City',
    'Tottenham Hotspur',
    'Watford',
    'West Bromwich Albion',
    'West Ham United'
];

$data = [
    [
        'odds' => 20.7,
        'stake' => 100,
        'winnings' => "18404",
        'status' => 'open',
        'betcode' => '1811300362',
        'games' => [

            ["match" => "chelsea - Manu", "prediction" => "GG"],
            ["match" => "Asernal - Barca", "prediction" => "GG"],
            ["match" => "Liverpool - Bayern", "prediction" => "over2.5"],
            ["match" => "Everton - Wartford", "prediction" => "Over1.5"],
        ],
        'no_of_games' => 4,
    ],
    [
        'odds' => 20.7,
        'winnings' => "4,840",
        'status' => 'open',
        'stake' => 100,
        'betcode' => '6603738619',
        'no_of_games' => 8,
        'games' => [
            ["match" => "chelsea - Manu", "prediction" => "GG"],
            ["match" => "Asernal - Barca", "prediction" => "GG"],
            ["match" => "Liverpool - Bayern", "prediction" => "over2.5"],
            ["match" => "Everton - Wartford", "prediction" => "Over1.5"],
            ["match" => "Roma -   Nice", "prediction" => "Over1.5"],
            ["match" => "Lyon - Ajax", "prediction" => "GG"],
            ["match" => "Nancy - Villareal", "prediction" => "12&GG"],
            ["match" => "Everton - Wartford", "prediction" => "12"],
        ],
    ],
    [
        'id' => '3',
        'odds' => 20.7,
        'winnings' => "18,404",
        'status' => 'open',
        'stake' => 100,
        'betcode' => '2206503904',
        'no_of_games' => 5,
        'games' => [
            ["match" => "chelsea - Celtic", "prediction" => "GG"],
            ["match" => "Asernal - Nice", "prediction" => "GG"],
            ["match" => "Liverpool - ", "prediction" => "over2.5"],
            ["match" => "lazio - Metz", "prediction" => "under1.5"],
            ["match" => "Gent - Hearts", "prediction" => " GG"],
        ],
    ],
    [
        'odds' => 50.4,
        'winnings' => 98842,
        'status' => 'open',
        'stake' => 1480,
        'betcode' => '1547041014',
        'no_of_games' => 3,
        'games' => [
            ["match" => "chelsea - Manu", "prediction" => "GG"],
            ["match" => "Asernal - Barca", "prediction" => "GG"],
            ["match" => "Real Madrid - Leganes", "prediction" => "12"],
        ],
    ],
    [
        'odds' => 50.4,
        'winnings' => "98,842",
        'status' => 'open',
        'stake' => 1480,
        'betcode' => '7660427017',
        'no_of_games' => 5,
        'games' => [
            ["match" => "Werder Bremen - Hertha Berlin", "prediction" => "12"],
            ["match" => "VfB Stuttgart - Schalke 04", "prediction" => "1"],
            ["match" => "FC Cologne - Augsburg", "prediction" => "2"],
            ["match" => "Paris Saint Germain - Montpellier", "prediction" => "12"],
            ["match" => "Metz - Nice", "prediction" => "1"],
        ],
    ],
    [
        'odds' => 15.3,
        'winnings' => 76500,
        'status' => 'open',
        'stake' => 5000,
        'betcode' => '2084198961',
        'no_of_games' => 7,
        'games' => [
            ["match" => "Millwall - Rochdale", "prediction" => "GG"],
            ["match" => "Huddersfield Town - Birmingham City", "prediction" => "GG"],
            ["match" => "Zenit St. Petersburg - Slavia Prague", "prediction" => "12"],
            ["match" => "Sassuolo - Atalanta", "prediction" => "12"],
            ["match" => "Bristol City - Queens Park Rangers", "prediction" => "3"],
            ["match" => "Brentford - Norwich City", "prediction" => "2X"],
            ["match" => "Barnsley - Fulham", "prediction" => "over 2.5"],
        ],
    ],

];

$request = $_GET;
if ( array_key_exists('betcode', $request) )
{

    foreach ($data as $rec)
    {
        if ( $rec['betcode'] == $request['betcode'] )
        {
            header('Content-Type: application/json');
            echo json_encode($rec);
        }
    }
    exit();
}

$json = json_encode($data);
header('Content-Type: application/json');
echo($json);
?>
