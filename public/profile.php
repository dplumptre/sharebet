<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> 

<html class="no-js" lang="en"> <!--<![endif]-->
<head>

 <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>ShareBet</title>
<meta name="description" content="">
<meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Roboto:200,400,300,900' rel='stylesheet' type='text/css'>

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<link rel="stylesheet" href="lib/gridiculous.css">
<link rel="stylesheet" href="lib/menu.css">
<link rel="stylesheet" href="lib/style.css">


<!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="img/png" href="img/favicon.png">
  
<!-- Script
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<!--<script src="js/jquery.js"></script>-->
    <!-- Include the Sidr JS -->
     <script src="js/jquery-2.1.1.js"></script>

<!--<script src="js/modernizr.js"></script>--> <!-- Modernizr -->

</head>



<body>
<!-- Header Content From BetSite –––––––––––––––––––––––––––––––––––––––––––––––––– -->

<div class="grid wfull betheader"></div>
	
<!-- Header Content From BetSite END–––––––––––––––––––––––––––––––––––––––––––––––--->

<!-- Main Banner –––––––––––––––––––––––––––––––––––––––––––––––––– -->

<div class="grid wfull mainbanner"></div>
	
<!-- Main Banner END–––––––––––––––––––––––––––––––––––––––––––––––--->

<!-- Global Menu –––––––––––––––––––––––––––––––––––––––––––––––––– -->

<div class="grid wfull globalmenu">
<div id="gmenu">
<div id='cssmenu'>
<ul>
   <li><a href='index.php'><span>Discover Betslips</span></a></li>
   <li><a href='bettors.php'><span>Bettors Leaderboard</span></a></li>
   <li class='last active'><a href='profile.php'><span>My Bets <img src="img/mainprofilepic.png"></span></a></li>
</ul>
</div>
</div>
</div>
	
<!-- Global Menu END–––––––––––––––––––––––––––––––––––––––––––––––--->

<!-- Full Page –––––––––––––––––––––––––––––––––––––––––––––––––– -->

<div class="grid wfull page1 mgtop">
<div class="grid w1080">

	<div class="row">
<!-- Panels –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div class="row">
<!-- Left Panel –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div class="c3 profileleft">
<div class="c12 profiledetail">
	
        <table cellpadding="10px" class="profiledhead ticketsize">
	<tbody>

<tr>
<td style="width: 100%;"><img src="img/userpic.png" style="max-width:100%;height:auto;"></td>
</tr>

<tr>
<td style="width: 100%;">
<font class="usernamepro">naijaronaldo</font>
<br>
<font class="userlevelpro">1st Team Shirt</font></td>
</tr>
	</tbody>
	</table>



        <table cellpadding="10px" class="profiledhead2 ticketsize">
	<tbody>

<tr>
<td style="width: 100%;">
<font class="profollowing">510</font>
<br>
<font class="protitle">FOLLOWERS</font></td>
</tr>

<tr>
<td style="width: 100%;">
<button-follow>FOLLOW</button-follow>
<br><br>
<font class="propeflost">&bull;</font>
<font class="propefwon">&bull;</font>
<font class="propefwon">&bull;</font>
<font class="propeflost">&bull;</font>
<font class="propeflost">&bull;</font></td>
</tr>
	</tbody>
	</table>
        
        
        
        
        <table cellpadding="10px" class="profiledhead2 ticketsize">
	<tbody>

<tr>
<td style="width: 100%;">
<font class="protitle">BADGES</font></td>
</tr>

<tr>
<td style="width: 100%;">&nbsp;</td>
</tr>
	</tbody>
	</table>

</div>
</div>
<!-- Left Panel END –––––––––––––––––––––––––––––––––––––––––––––– -->

<!-- Mid Panel –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div class="c6 profilemid">

<div class="row">
	<div class="row">
<div class="c12 profilegamedata">
	
<p>        <div class="c4 profiledivider2">
<font class="promaindata">85</font>
<br>
<font class="prodatatitle">SHARED BET</font>
	</div>

        <div class="c4 profiledivider">
<font class="promaindata">67%</font>
<br>
<font class="prodatatitle">WIN RATE</font>
	</div>

        <div class="c4 profiledivider2">
<font class="promaindata">₦52,700</font>
<br>
<font class="prodatatitle">TOTAL WINNINGS</font>
	</div>
</div>
	</div>

	<div class="row">
<div class="c12 profilegamehistory">BET HISTORY</div>
	</div>
</div>

<!-- Ticket Panel –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<div class="row">
<div class="c4 profileticket">
<table class="ticketopenhead ticketsize">
<tbody>
<tr>
<td style="width: 35%;"><img src="img/profilepic7.png" style="max-width:100%;height:auto;"></td>
<td style="width: 65%;">
<table style="max-width: 100%; height: 100%;">
<tbody>
<tr>
<td>
<font class="usernameatt">naijaronaldo</font>
<br>
<font class="userlevelatt">1st Team Shirt</font>
</td>
</tr>
<tr>
<td>
<table class="ticketfftab ticketffsize">
<tbody>
<tr>
<td style="width: 60%; text-align:left;">
<button>FOLLOW</button>
<br>
<font class="peflost">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="peflost">&bull;</font>
<font class="peflost">&bull;</font>
</td>
<td style="width: 40%; text-align:right;">
<font class="rightminititle">Status:</font>
<br>
<font class="ticketopenstat">Open</font>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>

<table cellpadding="5px" class="ticketoddtable ticketoddsize">
<tbody>
<tr>
<td style="width: 50%; text-align:left;">
<font class="rightminititle">Stake:</font>
<br>
<font class="odddata1">₦100</font>
</td>
<td style="width: 50%; text-align:right;">
<font class="rightminititle">Winnings:</font>
<br>
<font class="windata">₦321,515</font>
</td>
</tr>
<tr>
<td style="width: 50%; text-align:left;">
<font class="rightminititle">Odd:</font>
<br>
<font class="odddata2">3125.15</font>
</td>
<td style="width: 50%; text-align:right;">
<font class="rightminititle">Games:</font>
<br>
<font class="odddata2">15</font>
</td>
</tr>
</tbody>
</table>
<table cellpadding="5px" class="ticketoddtable ticketoddsize2">
<tbody>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">West Brom - Crystal Palace</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">GG</font></td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Brighton - Liverpool</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">2</font></td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Leicester City - Burnley</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">12</font></td>
</tr>

</tbody>
</table>
<table class="rebetbuttab rebetbuttsize">
<tbody>
<tr>
<td style="width: 60%; background-color:#089B7F;"><button-rebet>REBET TICKET</button-rebet></td>
<td style="width: 30%; text-align:center; background-color:#D2D3D5;">
<font class="rebetcount">&infin; 98</font>
</td>
<td style="width: 10%; background-color:#2C3E50;"><button-drop>&or;</button-rebet></td>
</tr>
</tbody>
</table>
</div>
<!-- –––––––––––––––––––––––––––––––––––----------DIVIDER---------------------------------––––––––––––––– -->
<div class="c4 profileticket">
<table class="ticketopenhead ticketsize">
<tbody>
<tr>
<td style="width: 35%;"><img src="img/profilepic7.png" style="max-width:100%;height:auto;"></td>
<td style="width: 65%;">
<table style="max-width: 100%; height: 100%;">
<tbody>
<tr>
<td>
<font class="usernameatt">naijaronaldo</font>
<br>
<font class="userlevelatt">1st Team Shirt</font>
</td>
</tr>
<tr>
<td>
<table class="ticketfftab ticketffsize">
<tbody>
<tr>
<td style="width: 60%; text-align:left;">
<button>FOLLOW</button>
<br>
<font class="peflost">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="peflost">&bull;</font>
<font class="peflost">&bull;</font>
</td>
<td style="width: 40%; text-align:right;">
<font class="rightminititle">Status:</font>
<br>
<font class="ticketopenstat">Open</font>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>

<table cellpadding="5px" class="ticketoddtable ticketoddsize">
<tbody>
<tr>
<td style="width: 50%; text-align:left;">
<font class="rightminititle">Stake:</font>
<br>
<font class="odddata1">₦100</font>
</td>
<td style="width: 50%; text-align:right;">
<font class="rightminititle">Winnings:</font>
<br>
<font class="windata">₦321,515</font>
</td>
</tr>
<tr>
<td style="width: 50%; text-align:left;">
<font class="rightminititle">Odd:</font>
<br>
<font class="odddata2">3125.15</font>
</td>
<td style="width: 50%; text-align:right;">
<font class="rightminititle">Games:</font>
<br>
<font class="odddata2">15</font>
</td>
</tr>
</tbody>
</table>
<table cellpadding="5px" class="ticketoddtable ticketoddsize2">
<tbody>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">West Brom - Crystal Palace</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">GG</font></td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Brighton - Liverpool</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">2</font></td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Leicester City - Burnley</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">12</font></td>
</tr>

</tbody>
</table>
<table class="rebetbuttab rebetbuttsize">
<tbody>
<tr>
<td style="width: 60%; background-color:#089B7F;"><button-rebet>REBET TICKET</button-rebet></td>
<td style="width: 30%; text-align:center; background-color:#D2D3D5;">
<font class="rebetcount">&infin; 98</font>
</td>
<td style="width: 10%; background-color:#2C3E50;"><button-drop>&or;</button-rebet></td>
</tr>
</tbody>
</table>
</div>
<!-- –––––––––––––––––––––––––––––––––––----------DIVIDER---------------------------------––––––––––––––– -->
<div class="c4 profileticket">
<table class="ticketopenhead ticketsize">
<tbody>
<tr>
<td style="width: 35%;"><img src="img/profilepic7.png" style="max-width:100%;height:auto;"></td>
<td style="width: 65%;">
<table style="max-width: 100%; height: 100%;">
<tbody>
<tr>
<td>
<font class="usernameatt">naijaronaldo</font>
<br>
<font class="userlevelatt">1st Team Shirt</font>
</td>
</tr>
<tr>
<td>
<table class="ticketfftab ticketffsize">
<tbody>
<tr>
<td style="width: 60%; text-align:left;">
<button>FOLLOW</button>
<br>
<font class="peflost">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="peflost">&bull;</font>
<font class="peflost">&bull;</font>
</td>
<td style="width: 40%; text-align:right;">
<font class="rightminititle">Status:</font>
<br>
<font class="ticketopenstat">Open</font>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>

<table cellpadding="5px" class="ticketoddtable ticketoddsize">
<tbody>
<tr>
<td style="width: 50%; text-align:left;">
<font class="rightminititle">Stake:</font>
<br>
<font class="odddata1">₦100</font>
</td>
<td style="width: 50%; text-align:right;">
<font class="rightminititle">Winnings:</font>
<br>
<font class="windata">₦321,515</font>
</td>
</tr>
<tr>
<td style="width: 50%; text-align:left;">
<font class="rightminititle">Odd:</font>
<br>
<font class="odddata2">3125.15</font>
</td>
<td style="width: 50%; text-align:right;">
<font class="rightminititle">Games:</font>
<br>
<font class="odddata2">15</font>
</td>
</tr>
</tbody>
</table>
<table cellpadding="5px" class="ticketoddtable ticketoddsize2">
<tbody>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">West Brom - Crystal Palace</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">GG</font></td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Brighton - Liverpool</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">2</font></td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Leicester City - Burnley</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">12</font></td>
</tr>

</tbody>
</table>
<table class="rebetbuttab rebetbuttsize">
<tbody>
<tr>
<td style="width: 60%; background-color:#089B7F;"><button-rebet>REBET TICKET</button-rebet></td>
<td style="width: 30%; text-align:center; background-color:#D2D3D5;">
<font class="rebetcount">&infin; 98</font>
</td>
<td style="width: 10%; background-color:#2C3E50;"><button-drop>&or;</button-rebet></td>
</tr>
</tbody>
</table>
</div>
<!-- –––––––––––––––––––––––––––––––––––----------DIVIDER---------------------------------––––––––––––––– -->
	</div>
<!-- Ticket Panel END–––––––––––––––––––––––––––––––––––––––––––––––––– -->

</div>
<!-- Mid Panel END –––––––––––––––––––––––––––––––––––––––––––––– -->

<!-- Right Panel –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div class="c3 profileright">
<div class="row mgbt">
<div class="c10 shareabet">
<!-- Sharebet Panel –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<div class="row">
<div class="c12 heading1">SHARE YOUR TICKET</div>
	</div>

	<div class="row">
<div class="c12 sharebox">
<form style="text-align:left;">
  <p>Insert Bet Code:<br><input style="width:100%; text-transform:uppercase;" name="name" type="text" value="" /></p>
  <p>Add a Comment (Optional):<br><textarea cols="30" rows="2">Long text.</textarea><p/> 
</form>
<p align="center"><button-sharebet>SHAREBET</button-sharebet></p>
</div>
	</div>
</div></div>

</div>
<!-- Right Panel END –––––––––––––––––––––––––––––––––––––––––––––– -->
	</div>









</div>	

</div>
</div>

<!-- Full Page END–––––––––––––––––––––––––––––––––––––––––––––––––– -->

<!-- Footer –––––––––––––––––––––––––––––––––––––––––––––––––– -->

<div class="grid wfull footer">
<div class="grid w1080">

        <div class="row">
<div class="c12 footerin">
<p>© 2017 | Sharbet Inc. | All Rights Reserved</p>
</div>
	</div>	

        
</div>
</div>
	

<!-- Footer END –––––––––––––––––––––––––––––––––––––––––––––––––– ->


</body>
</html>