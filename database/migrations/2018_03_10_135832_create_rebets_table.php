<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRebetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rebets', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('betcode_id');
            $table->unsignedInteger('user_id');
            $table->timestamps();

            $table->index('betcode_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rebets');
    }
}
