<?php

namespace App\Http\Requests;

use App\Models\Game;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\json_decode;
use Illuminate\Foundation\Http\FormRequest;

class SharebetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'betcode' => 'required|unique:betcodes',
            'comment' => 'nullable'
        ];
    }


    public function save()
    {
        $data = $this->queryApi();
        if ( is_null($data) ):
            notify()->flash('You supplied an invalid betcode. Please check and try again', 'error');
        else:
            $this->saveModel($data);
            notify()->flash('Your betcode has been shared successfully', 'success');
        endif;
    }


    private function saveModel(\stdClass $data)
    {
        $betCodeModel = auth()->user()->betcodes()->create([
            'betcode'       => $data->betcode,
            'status'        => $data->status,
            'stake'         => $data->stake,
            'winnings'      => $data->winnings,
            'odds'          => $data->odds,
            'no_of_games'   => count($data->games),
        ]);
        $games=[];
        foreach ($data->games as $game)
        {
            $games[] = new Game(['match'=>$game->match, 'prediction'=>$game->prediction]);
        }
        $betCodeModel->games()->saveMany($games);
    }


    private function queryApi()
    {
        $client = new Client();
        $response = $client->request('GET', config('sharebet.api_url').'/?betcode='.$this->betcode);
        $body = (string) $response->getBody();
        return json_decode($body);
    }
}
