<?php

namespace App\Http\Requests\API;

use App\Models\Game;
use GuzzleHttp\Client;
use Illuminate\Foundation\Http\FormRequest;

class SharebetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'betcode' => 'required|unique:betcodes',
            'comment' => 'nullable'
        ];
    }


    public function save()
    {
        $data = $this->queryApi();
        if (is_null($data)):
            $errors = ['errors' => ['betcode' => ['Invalid betcode supplied']]];
            return response()->json('', 422);
        else:
            $this->saveModel($data);
            return response()->json('success');
        endif;
    }


    private function saveModel(\stdClass $data)
    {
        $betCodeModel = auth()->user()->betcodes()->create([
            'betcode' => $data->betcode,
            'status' => $data->status,
            'stake' => $data->stake,
            'winnings' => $data->winnings,
            'odds' => $data->odds,
            'no_of_games' => count($data->games),
        ]);
        $games = [];
        foreach ($data->games as $game) {
            $games[] = new Game(['match' => $game->match, 'prediction' => $game->prediction]);
        }
        $betCodeModel->games()->saveMany($games);
    }


    private function queryApi()
    {
        $client = new Client();
        $response = $client->request('GET', config('sharebet.api_url') . '/?betcode=' . $this->betcode);
        $body = (string)$response->getBody();
        return json_decode($body);
    }
}
