<?php

namespace App\Http\Controllers;

use App\Models\Betcode;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{


    public function index()
    {
        $data = Betcode::with('user', 'games')->latest()->paginate(8);
        $chunk = 4;
        return view('welcome/welcome', compact('chunk', 'data'));
        // return view('layouts/app');
    }


    public function profile()
    {
        $data = auth()->user()->betcodes()->with('games', 'user')->paginate();
        $chunk = 3;
        return view('welcome/profile', compact('data', 'chunk'));
    }


}
