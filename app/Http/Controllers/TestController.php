<?php

namespace App\Http\Controllers;

use App\Models\DemoBetcode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class TestController extends Controller
{

    public function external(Request $request)
    {
        $query = $request->query('betcode');

        $row = DB::table('demo_betcodes')
            ->select('id', 'betcode', 'odds', 'stake', 'winnings', 'status', 'expire_at')
            ->where('betcode', $query)
            ->first();
        if (is_null($row))
            return null;
        $row2 = DB::table('demo_betcode_games')
            ->select('match', 'prediction')
            ->where('demo_betcode_id', $row->id)
            ->get();

        unset($row->id);
        $row->games = $row2;

        return json_encode($row);
    }


    public function sampleGames()
    {
        $data = DemoBetcode::orderBy('expire_at', 'DESC')->latest()->paginate(10);
        return view('sample-games', compact('data'));
    }

    public function postSampleGames(Request $request)
    {
        $data = $request->validate([
            'count' => 'required',
            'truncate' => 'required',
            'expire_in' => 'required'
        ]);

        $betcode = Artisan::call('sharebet:demodata', [
            'count' => $data['count'],
            'truncate' => $data['truncate'],
            'expire_in' => $data['expire_in']
        ]);
        flash('Betcodes generated', 'success');
        return back();
    }
}
