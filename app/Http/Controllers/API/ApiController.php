<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\SharebetRequest;
use App\Models\Betcode;
use App\Models\Rebet;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{

    /**
     * Method receives a 'Post' betcode & comment which
     * is further retrieved over an API
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postQueryBet(Request $request)
    {
        //validate
        $data = $request->validate([
            'betcode' => 'required|unique:betcodes',
            'comment' => 'nullable'
        ]);

        //query external API
        $data = $this->queryApi($data['betcode']);

        if ($data):
            //return data
            return response()->json($data);
        else:
            $errors = ['errors' => ['betcode' => ['Invalid betcode supplied']]];
            return response()->json($errors, 422);
        endif;
    }


    /**
     * Method receives betcode & comment input to check teh betcode
     * over an external API and save the betcode details to table
     * @param SharebetRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postShareBet(SharebetRequest $request)
    {
        return $request->save();
    }


    /**
     * Gets the requesting user's shared betcodes along with games
     * @param Request $request
     * @return mixed
     */
    public function getUserSharedBetCodes(Request $request)
    {
        return auth()->guard('api')->user()->betcodes()->with('games')->get();
    }


    /**
     * Gets the betcode details from the supplied betcode id
     * @param null $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getQueryBet($id=null)
    {
        if ( $id === null )
            return response()->json(['error'], 401);
        $data = Betcode::with('games')->find((int) $id);
        return $data;
    }


    /**
     * Private method to query and external API
     * @param $betcode
     * @return string
     */
    private function queryApi($betcode)
    {
        $client = new Client();
        $response = $client->request('GET', config('sharebet.api_url').'/?betcode='.$betcode);
        $body = (string) $response->getBody();
        return $body;
    }


    public function rebet(Request $request)
    {
        //validate
        $data = $request->validate([
            'betcode_id' => 'required',
            'stake' => 'required',
        ]);

        //save rebet
        auth()->user()->rebets()->create([
            'betcode_id' => $data['betcode_id'],
        ]);

        //share betcode
        return 'success';
    }


}
