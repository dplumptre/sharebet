<?php

namespace App\Http\Controllers;

use App\Http\Requests\SharebetRequest;
use App\Models\Betcode;
use App\Models\Game;
use GuzzleHttp\json_decode;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class ShareBetController extends Controller
{


    public function index()
    {
        return view('sharebet.index');
    }


    /**
     * @param Request $request
     */
    public function create(Request $request, SharebetRequest $sharebetRequest)
    {
        $sharebetRequest->save();
        return back();
    }
}
