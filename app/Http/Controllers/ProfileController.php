<?php

namespace App\Http\Controllers;

use App\Models\Betcode;
use App\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{


    /**
     * ProfileController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth')->except('logout');
    }

    public function tickets()
    {
        $data = Betcode::with('user', 'games')->latest()->paginate(8);
        $chunk = 4;
        return view('welcome/welcome', compact('chunk', 'data'));
    }


    public function bettors()
    {

    }


    public function highOdds()
    {
        $data = Betcode::with('user', 'games')->orderBy('odds', 'DESC')->latest()->paginate(8);
        $chunk = 4;
        return view('welcome/welcome', compact('chunk', 'data'));
    }


    public function lowOdds()
    {
        $data = Betcode::with('user', 'games')->orderBy('odds', 'ASC')->latest()->paginate(8);
        $chunk = 4;
        return view('welcome/welcome', compact('chunk', 'data'));
    }


    public function profile()
    {
        $data = auth()->user()->betcodes()->with('games', 'user', 'rebets')->latest()->paginate();
        $chunk = 3;
        return view('welcome/profile', compact('data', 'chunk'));
    }


    public function anotherUser($username, Request $request)
    {
        $user = User::where('username', $username)->first();

        if (auth()->user()->id === $user->id)
            return back();

        $data = Betcode::with(['games'])->where('user_id', $user->id)->paginate();
        //dd($data, $user);
        $chunk = 3;
        return view('profile.another-user', compact('chunk', 'user', 'data'));
    }
}
