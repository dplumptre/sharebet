<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StartController extends Controller
{


    public function index(Request $request)
    {
        $username = $request->query('username');
        $user = User::getByUsername($username);
        if ( $user )
        {
            Auth::login($user);
            return redirect()->route('profile');
        }
        return view('start.register', compact('username'));
        //check if username exists
    }


    public function postRegister(Request $request)
    {
        $data = $request->validate([
            'username'  => 'required|unique:users',
            'email'     => 'required|email|unique:users',
            'phone'     => 'required|min:11|max:13|unique:users'
        ]);

        $user = User::create([
            'username' => $data['username'],
            'email' => $data['email'],
            'phone' => $data['phone'],
        ]);

        Auth::login($user);
        return redirect('home');
    }
}
