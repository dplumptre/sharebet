<?php

use Illuminate\Http\Request;

function nicknames($nickname = null)
{
    $nicknames = [
        '1st team shirt',
        'muntula',
        'egbeji 1',
        'sure boy',
        'action boy 1',
        'student',
        'helper',
    ];

    if (!is_null($nickname) && in_array($nickname, $nicknames)) {
        return $nickname;
    } else {
        $start = 0;
        $end = count($nicknames) - 1;
        $name = random_int($start, $end);
        return $nicknames[$name];
    }
}

function float_rand($min, $Max, $round = 0)
{
    //validate input
    if ($min > $Max) {
        $min = $Max;
        $max = $min;
    } else {
        $min = $min;
        $max = $Max;
    }
    $randomfloat = $min + mt_rand() / mt_getrandmax() * ($max - $min);
    if ($round > 0)
        $randomfloat = round($randomfloat, $round);

    return $randomfloat;
}

function generate_betcode()
{
    return random_int(1111111111, (int)9999999999);
}

function mark_url_active($route)
{
    return is_url($route) ? ' active' : '';
}
function is_url($route)
{
    //check if we don not have a full url. often from route('url')
    if ( substr($route, 0, 4) !== 'http' ){
        //get it
        $route = route($route);
    }

    if (app('request')->url() == $route)
        return true;
    else
        return false;
}
