<?php
/**
 * Created by PhpStorm.
 * User: segun
 * Date: 12/29/2017
 * Time: 1:44 PM
 */

namespace App\Observers;


use App\User;

class UserObserver
{

    public function creating(User $user)
    {
        $user->api_token = str_random(60);
    }

}