<?php

namespace App;

use App\Models\ActiveRelationship;
use App\Models\Betcode;
use App\Models\Comment;
use App\Models\Rebet;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function betcodes()
    {
        return $this->hasMany(Betcode::class);
    }


    public function comments()
    {
        return $this->hasMany(Comment::class);
    }


    //returns followers
    public function followers()
    {
        return $this->hasMany(ActiveRelationship::class, 'followed_id', 'id');
    }


    public function following()
    {
        return $this->hasMany(ActiveRelationship::class, 'follower_id', 'id');
    }


    //returns followed users (people being follwed)
    public function followed()
    {
        return $this->hasMany(ActiveRelationship::class, 'followed_id', 'id');
    }


    public static function getByUsername($username)
    {
        return (new self)->where('username', $username)->first();
    }


    public function follow(User $other_user)
    {
        dd($other_user);
    }


    public function unfollow(User $other_user)
    {

    }


    public function isFollowing(User $user)
    {
        return ActiveRelationship::select('id')->where('follower_id', $this->id)->where('followed_id', $user->id)->first();
    }


    public function totalWinnings()
    {
        return $this->betcodes()->where('status', 'won')->sum('winnings');
    }


    public function winRate()
    {
        $wins = $this->betcodes()->where('status', 'won')->count();
        $all = $this->betcodes()->count();

        if ( $all > 0 )
            return (int) (($wins/$all) * 100);
        else
            return 0;
    }


    public function rebets()
    {
        return $this->hasMany(Rebet::class);
    }

}
