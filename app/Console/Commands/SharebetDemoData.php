<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SharebetDemoData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sharebet:demodata {count=20} {truncate?} {expire_in?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populate the sharebet table with data to display for the demo API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $truncate = $this->argument('truncate');
        $count = $this->argument('count');
        $expire_in = $this->argument('expire_in');
        $bar = $this->output->createProgressBar($count);

        $table_name = 'demo_betcodes';
        $table2_name = 'demo_betcode_games';
        //truncate table
        if ($truncate === 'true'):
            DB::table($table2_name)->truncate();
            DB::table($table_name)->truncate();
        endif;



        for ($j = 1; $j <= $count; $j++) {
            //prepare data
            $odds = float_rand(0.5, 5.5, 2);
            $stake = random_int(100, 2000);
            $winnings = round($odds * $stake, 2);
            $expire =  !empty($expire_in) ? time() + ((int) $expire_in * 60) : time() + (random_int(2, 20) * 60);
            $now = Carbon::now();
            $row = DB::table($table_name)->insertGetId(
                [
                    'betcode' => generate_betcode(),
                    'odds' => $odds,
                    'stake' => $stake,
                    'winnings' => $winnings,
                    'status' => 'open',
                    'expire_at' => $expire,
                    'created_at' => $now,
                    'updated_at' => $now,
                ]
            );
            $team_count = count($this->teams());
            $max_games = (int)$team_count / 2;
            $no_of_games = random_int(1, $max_games);
            $games = [];
            $now = Carbon::now();
            for ($i = 0; $i <= $no_of_games - 1; $i++) {
                DB::table($table2_name)->insert([
                    'demo_betcode_id' => $row,
                    'match' => $this->game(),
                    'prediction' => $this->prediction(),
                    'created_at' => $now,
                    'updated_at' => $now,
                ]);
            }
            $bar->advance();
        }
        $bar->finish();
        echo "\n";
    }


    private function prediction()
    {
        $data = [
            '1x2',
            '1',
            'X',
            '2',
            '1X',
            'X2',
            '12',
            'O/U2.5',
            '2.5 O',
            '2.5 U',
            'HF 2/2',
            'HF 2/X',
            'HF 2/1',
            'HF X/2',
            'HF X/X',
            'HF X/1',
            'HF 1/2',
            'HF 1/X',
            'HF 1/1'
        ];
        $max = count($data);
        $r = random_int(0, $max - 1);
        return $data[$r];
    }

    private function teams()
    {
        $teams = [
            'AFC Bournemouth',
            'Arsenal',
            'Brighton & Hove Albion',
            'Burnley',
            'Chelsea',
            'Crystal Palace',
            'Everton',
            'Huddersfield Town',
            'Leicester City',
            'Liverpool',

            'Manchester City',
            'Manchester United',
            'Newcastle United',
            'Southampton',
            'Stoke City',
            'Swansea City',
            'Tottenham Hotspur',
            'Watford',
            'West Bromwich Albion',
            'West Ham United'
        ];
        return $teams;
    }

    private function game()
    {
        $teams = $this->teams();
        $count = count($teams);
        $half = $count / 2;

        //first half
        $half1 = $teams[random_int(0, $half - 1)];

        //second half
        $half2 = $teams[random_int($half, $count - 1)];

        //output
        return "$half1 vs $half2";
    }
}
