<?php

namespace App\Console\Commands;

use App\Models\Betcode;
use App\Models\DemoBetcode;
use Illuminate\Console\Command;

class ModifyBetcodeStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sharebet:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Modify sharebet status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $betcodes = Betcode::where('status', 'open')->get();
        $status = ['won', 'lost'];
        $counter = 0;

        //dd($betcodes);

        foreach ($betcodes as $betcode)
        {
            $demoBetcode = DemoBetcode::where('betcode', $betcode->betcode)
                ->where('expire_at', '<=', time())
                ->first();

            if ($demoBetcode){
                $betcode->status = $status[random_int(0,1)];
                $betcode->save();
            }
        }
       
       
    }
}
