<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['comment', 'approved'];


    public function betCode()
    {
        return $this->belongsTo(Betcode::class);
    }
}
