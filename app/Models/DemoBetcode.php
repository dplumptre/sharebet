<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DemoBetcode extends Model
{
    protected $table = 'demo_betcodes';
    protected $fillable = ['betcode', 'odds', 'stake', 'winnings', 'status', 'expire_at'];
}
