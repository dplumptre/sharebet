<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Rebet extends Model
{
    protected $fillable = ['betcode_id'];

    public function betcodes()
    {
        return $this->belongsTo(Betcode::class);
    }


    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
