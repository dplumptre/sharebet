<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Betcode extends Model
{
    protected $table = 'betcodes';
    protected $fillable = ['betcode', 'status', 'stake', 'winnings', 'odds', 'no_of_games'];


    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function games()
    {
        return $this->hasMany(Game::class);
    }


    public function comments()
    {
        return $this->hasMany(Comment::class);
    }


    public function rebets()
    {
        return $this->hasMany(Rebet::class);
    }


    public function updateStatus($status)
    {
        $this->update([
        'status' => $status
        ]);
    }
}
