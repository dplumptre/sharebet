<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> 

<html class="no-js" lang="en"> <!--<![endif]-->
<head>

 <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>ShareBet</title>
<meta name="description" content="">
<meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Roboto:200,400,300,900' rel='stylesheet' type='text/css'>

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<link rel="stylesheet" href="lib/gridiculous.css">
<link rel="stylesheet" href="lib/menu.css">
<link rel="stylesheet" href="lib/style.css">


<!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="img/png" href="img/favicon.png">
  
<!-- Script
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<!--<script src="js/jquery.js"></script>-->
    <!-- Include the Sidr JS -->
     <script src="js/jquery-2.1.1.js"></script>

<!--<script src="js/modernizr.js"></script>--> <!-- Modernizr -->

</head>



<body>
<!-- Header Content From BetSite –––––––––––––––––––––––––––––––––––––––––––––––––– -->

<div class="grid wfull betheader"></div>
	
<!-- Header Content From BetSite END–––––––––––––––––––––––––––––––––––––––––––––––--->

<!-- Main Banner –––––––––––––––––––––––––––––––––––––––––––––––––– -->

<div class="grid wfull mainbanner"></div>
	
<!-- Main Banner END–––––––––––––––––––––––––––––––––––––––––––––––--->

<!-- Global Menu –––––––––––––––––––––––––––––––––––––––––––––––––– -->

<div class="grid wfull globalmenu">
<div id="gmenu">
<div id='cssmenu'>
<ul>
   <li class='active'><a href='index.php'><span>Discover Betslips</span></a></li>
   <li><a href='bettors.php'><span>Bettors Leaderboard</span></a></li>
   <li class='last'><a href='profile.php'><span>My Bets <img src="img/mainprofilepic.png"></span></a></li>
</ul>
</div>
</div>
</div>
	
<!-- Global Menu END–––––––––––––––––––––––––––––––––––––––––––––––--->

<!-- Full Page –––––––––––––––––––––––––––––––––––––––––––––––––– -->

<div class="grid wfull page1 mgtop">
<div class="grid w1080">

	<div class="row">
<!-- Left Panel –––––––––––––––––––––––––––––––––––––––––––––––––– -->

<div class="c9 leftpanel">

<!-- Filter Menu –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div class="row mgbt">
<div class="c12 filtermenu">
<div id='cssmenufilter'>
<ul>
   <li class='active'><a href='#'><span>All Tickets</span></a></li>
   <li><a href='#'><span>Bettors You Follow </span></a></li>
   <li><a href='#'><span>Most Rebets</span></a></li>
   <li><a href='#'><span>High Odds</span></a></li>
   <li class='last'><a href='#'><span>Low Odds</span></a></li>
</ul>
</div>
</div>
</div>
<!-- Filter Menu END–––––––––––––––––––––––––––––––––––––––––––––––––– -->

<!-- Tickets Container–––––––––––––––––––––––––––––––––––––––––––––––––– -->

<!---------------------------------------------------DIVIDER----------------–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div class="row mgbt">

<div class="c3 ticketcol1">
<table class="ticketopenhead ticketsize">
<tbody>
<tr>
<td style="width: 35%;"><img src="img/profilepic7.png" style="max-width:100%;height:auto;"></td>
<td style="width: 65%;">
<table style="max-width: 100%; height: 100%;">
<tbody>
<tr>
<td>
<font class="usernameatt">naijaronaldo</font>
<br>
<font class="userlevelatt">1st Team Shirt</font>
</td>
</tr>
<tr>
<td>
<table class="ticketfftab ticketffsize">
<tbody>
<tr>
<td style="width: 60%; text-align:left;">
<button>FOLLOW</button>
<br>
<font class="peflost">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="peflost">&bull;</font>
<font class="peflost">&bull;</font>
</td>
<td style="width: 40%; text-align:right;">
<font class="rightminititle">Status:</font>
<br>
<font class="ticketopenstat">Open</font>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>

<table cellpadding="5px" class="ticketoddtable ticketoddsize">
<tbody>
<tr>
<td style="width: 50%; text-align:left;">
<font class="rightminititle">Stake:</font>
<br>
<font class="odddata1">₦100</font>
</td>
<td style="width: 50%; text-align:right;">
<font class="rightminititle">Winnings:</font>
<br>
<font class="windata">₦321,515</font>
</td>
</tr>
<tr>
<td style="width: 50%; text-align:left;">
<font class="rightminititle">Odd:</font>
<br>
<font class="odddata2">3125.15</font>
</td>
<td style="width: 50%; text-align:right;">
<font class="rightminititle">Games:</font>
<br>
<font class="odddata2">15</font>
</td>
</tr>
</tbody>
</table>
<table cellpadding="5px" class="ticketoddtable ticketoddsize2">
<tbody>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">West Brom - Crystal Palace</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">GG</font></td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Brighton - Liverpool</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">2</font></td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Leicester City - Burnley</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">12</font></td>
</tr>

</tbody>
</table>
<table class="rebetbuttab rebetbuttsize">
<tbody>
<tr>
<td style="width: 60%; background-color:#089B7F;"><button-rebet>REBET TICKET</button-rebet></td>
<td style="width: 30%; text-align:center; background-color:#D2D3D5;">
<font class="rebetcount">&infin; 98</font>
</td>
<td style="width: 10%; background-color:#2C3E50;"><button-drop>&or;</button-rebet></td>
</tr>
</tbody>
</table>
</div>

<div class="c3 ticketcol1">
<table class="ticketlosthead ticketsize">
<tbody>
<tr>
<td style="width: 35%;"><img src="img/profilepic8.png" style="max-width:100%;height:auto;"></td>
<td style="width: 65%;">
<table style="max-width: 100%; height: 100%;">
<tbody>
<tr>
<td>
<font class="usernameatt">lackotcha</font>
<br>
<font class="userlevelatt">Wonderkid</font>
</td>
</tr>
<tr>
<td>
<table class="ticketfftab ticketffsize">
<tbody>
<tr>
<td style="width: 60%; text-align:left;">
<button>FOLLOW</button>
<br>
<font class="pefwon">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="peflost">&bull;</font>
<font class="peflost">&bull;</font>
</td>
<td style="width: 40%; text-align:right;">
<font class="rightminititle">Status:</font>
<br>
<font class="ticketloststat">Lost</font>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>

<table cellpadding="5px" class="ticketoddtable ticketoddsize">
<tbody>
<tr>
<td style="width: 50%; text-align:left;">
<font class="rightminititle">Stake:</font>
<br>
<font class="odddata1">₦500</font>
</td>
<td style="width: 50%; text-align:right;">
<font class="rightminititle">Winnings:</font>
<br>
<font class="windata">₦64,175</font>
</td>
</tr>
<tr>
<td style="width: 50%; text-align:left;">
<font class="rightminititle">Odd:</font>
<br>
<font class="odddata2">128.35</font>
</td>
<td style="width: 50%; text-align:right;">
<font class="rightminititle">Games:</font>
<br>
<font class="odddata2">8</font>
</td>
</tr>
</tbody>
</table>
<table cellpadding="5px" class="ticketoddtable ticketoddsize2">
<tbody>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Everton - Huddersfield Town</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">12&amp;GG</font></td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Stoke City - Swansea</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">Over 2.5</font></td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Arsenal - Manchester Utd</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">2X</font></td>
</tr>

</tbody>
</table>
<table class="rebetbuttab rebetbuttsize">
<tbody>
<tr>
<td style="width: 60%; background-color:#BDBFC1;"><button-rebet-na>REBET TICKET</button-rebet-na></td>
<td style="width: 30%; text-align:center; background-color:#D2D3D5;">
<font class="rebetcount">&infin; 15</font>
</td>
<td style="width: 10%; background-color:#2C3E50;"><button-drop>&or;</button-rebet></td>
</tr>
</tbody>
</table>
</div>

<div class="c3 ticketcol1">
<table class="ticketopenhead ticketsize">
<tbody>
<tr>
<td style="width: 35%;"><img src="img/profilepic1.png" style="max-width:100%;height:auto;"></td>
<td style="width: 65%;">
<table style="max-width: 100%; height: 100%;">
<tbody>
<tr>
<td>
<font class="usernameatt">walewoo</font>
<br>
<font class="userlevelatt">Super-Sub</font>
</td>
</tr>
<tr>
<td>
<table class="ticketfftab ticketffsize">
<tbody>
<tr>
<td style="width: 60%; text-align:left;">
<button>FOLLOW</button>
<br>
<font class="peflost">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="peflost">&bull;</font>
<font class="pefwon">&bull;</font>
</td>
<td style="width: 40%; text-align:right;">
<font class="rightminititle">Status:</font>
<br>
<font class="ticketopenstat">Open</font>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>

<table cellpadding="5px" class="ticketoddtable ticketoddsize">
<tbody>
<tr>
<td style="width: 50%; text-align:left;">
<font class="rightminititle">Stake:</font>
<br>
<font class="odddata1">₦500</font>
</td>
<td style="width: 50%; text-align:right;">
<font class="rightminititle">Winnings:</font>
<br>
<font class="windata">₦18,270</font>
</td>
</tr>
<tr>
<td style="width: 50%; text-align:left;">
<font class="rightminititle">Odd:</font>
<br>
<font class="odddata2">36.54</font>
</td>
<td style="width: 50%; text-align:right;">
<font class="rightminititle">Games:</font>
<br>
<font class="odddata2">5</font>
</td>
</tr>
</tbody>
</table>
<table cellpadding="5px" class="ticketoddtable ticketoddsize2">
<tbody>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Everton - Huddersfield Town</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">12&amp;GG</font></td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Stoke City - Swansea</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">Over 2.5</font></td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Arsenal - Manchester Utd</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">2X</font></td>
</tr>

</tbody>
</table>
<table class="rebetbuttab rebetbuttsize">
<tbody>
<tr>
<td style="width: 60%; background-color:#089B7F;"><button-rebet>REBET TICKET</button-rebet></td>
<td style="width: 30%; text-align:center; background-color:#D2D3D5;">
<font class="rebetcount">&infin; 105</font>
</td>
<td style="width: 10%; background-color:#2C3E50;"><button-drop>&or;</button-rebet></td>
</tr>
</tbody>
</table>
<table style="width: 200px;">
<tbody>
<tr>
<table cellpadding="5px" class="ticketoddtable ticketoddsize2">
<tbody>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 100%; text-align:left;">
<font class="commentusername">walewoo:</font>
<br>
<font class="commentdata">Arsenal haven't won in 10 of their Premier League home games</font>
</td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 100%; text-align:left;">
<font class="commentusername">kemisho:</font>
<br>
<font class="commentdata">This is very good!!!!!!</font>
</td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 100%; text-align:center;">
<font class="commentloaddata"><a href="#">- Load all comments -</a></font>
</td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 100%; text-align:center;">
<form>
<p><font class="commentusername"><a href="#">Comment on this ticket</a></font><br>
<input style="width:90%;" name="name" type="text" value="" /></p>
</form></td>
</tr>

</tbody>
</table>
</tr>
</tbody>
</table>
</div>

<div class="c3 ticketcol1">
<table class="ticketwonhead ticketsize">
<tbody>
<tr>
<td style="width: 35%;"><img src="img/profilepic6.png" style="max-width:100%;height:auto;"></td>
<td style="width: 65%;">
<table style="max-width: 100%; height: 100%;">
<tbody>
<tr>
<td>
<font class="usernameatt">Skrillex19</font>
<br>
<font class="userlevelatt">Super Star</font>
</td>
</tr>
<tr>
<td>
<table class="ticketfftab ticketffsize">
<tbody>
<tr>
<td style="width: 60%; text-align:left;">
<button>FOLLOW</button>
<br>
<font class="peflost">&bull;</font>
<font class="peflost">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="peflost">&bull;</font>
<font class="pefwon">&bull;</font>
</td>
<td style="width: 40%; text-align:right;">
<font class="rightminititle">Status:</font>
<br>
<font class="ticketwonstat">Won</font>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>

<table cellpadding="5px" class="ticketoddtable ticketoddsize">
<tbody>
<tr>
<td style="width: 50%; text-align:left;">
<font class="rightminititle">Stake:</font>
<br>
<font class="odddata1">₦1,200</font>
</td>
<td style="width: 50%; text-align:right;">
<font class="rightminititle">Winnings:</font>
<br>
<font class="windata">₦14,616</font>
</td>
</tr>
<tr>
<td style="width: 50%; text-align:left;">
<font class="rightminititle">Odd:</font>
<br>
<font class="odddata2">12.18</font>
</td>
<td style="width: 50%; text-align:right;">
<font class="rightminititle">Games:</font>
<br>
<font class="odddata2">3</font>
</td>
</tr>
</tbody>
</table>
<table cellpadding="5px" class="ticketoddtable ticketoddsize2">
<tbody>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Everton - Huddersfield Town</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">12&amp;GG</font></td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Stoke City - Swansea</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">Over 2.5</font></td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Arsenal - Manchester Utd</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">2X</font></td>
</tr>

</tbody>
</table>
<table class="rebetbuttab rebetbuttsize">
<tbody>
<tr>
<td style="width: 60%; background-color:#BDBFC1;"><button-rebet-na>REBET TICKET</button-rebet-na></td>
<td style="width: 30%; text-align:center; background-color:#D2D3D5;">
<font class="rebetcount">&infin; 4205</font>
</td>
<td style="width: 10%; background-color:#2C3E50;"><button-drop>&or;</button-rebet></td>
</tr>
</tbody>
</table>
</div>
</div>

<!---------------------------------------------------DIVIDER----------------–––––––––––––––––––––––––––––––––––––––––––––––––– -->

<div class="row mgbt">

<div class="c3 ticketcol1">
<table class="ticketopenhead ticketsize">
<tbody>
<tr>
<td style="width: 35%;"><img src="img/profilepic5.png" style="max-width:100%;height:auto;"></td>
<td style="width: 65%;">
<table style="max-width: 100%; height: 100%;">
<tbody>
<tr>
<td>
<font class="usernameatt">thanos9</font>
<br>
<font class="userlevelatt">Pro</font>
</td>
</tr>
<tr>
<td>
<table class="ticketfftab ticketffsize">
<tbody>
<tr>
<td style="width: 60%; text-align:left;">
<button>FOLLOW</button>
<br>
<font class="peflost">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="peflost">&bull;</font>
<font class="peflost">&bull;</font>
<font class="peflost">&bull;</font>
</td>
<td style="width: 40%; text-align:right;">
<font class="rightminititle">Status:</font>
<br>
<font class="ticketopenstat">Open</font>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>

<table cellpadding="5px" class="ticketoddtable ticketoddsize">
<tbody>
<tr>
<td style="width: 50%; text-align:left;">
<font class="rightminititle">Stake:</font>
<br>
<font class="odddata1">₦100</font>
</td>
<td style="width: 50%; text-align:right;">
<font class="rightminititle">Winnings:</font>
<br>
<font class="windata">₦321,515</font>
</td>
</tr>
<tr>
<td style="width: 50%; text-align:left;">
<font class="rightminititle">Odd:</font>
<br>
<font class="odddata2">3125.15</font>
</td>
<td style="width: 50%; text-align:right;">
<font class="rightminititle">Games:</font>
<br>
<font class="odddata2">15</font>
</td>
</tr>
</tbody>
</table>
<table cellpadding="5px" class="ticketoddtable ticketoddsize2">
<tbody>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">West Brom - Crystal Palace</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">GG</font></td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Brighton - Liverpool</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">2</font></td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Leicester City - Burnley</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">12</font></td>
</tr>

</tbody>
</table>
<table class="rebetbuttab rebetbuttsize">
<tbody>
<tr>
<td style="width: 60%; background-color:#089B7F;"><button-rebet>REBET TICKET</button-rebet></td>
<td style="width: 30%; text-align:center; background-color:#D2D3D5;">
<font class="rebetcount">&infin; 98</font>
</td>
<td style="width: 10%; background-color:#2C3E50;"><button-drop>&or;</button-rebet></td>
</tr>
</tbody>
</table>
</div>

<div class="c3 ticketcol1">
<table class="ticketlosthead ticketsize">
<tbody>
<tr>
<td style="width: 35%;"><img src="img/profilepic4.png" style="max-width:100%;height:auto;"></td>
<td style="width: 65%;">
<table style="max-width: 100%; height: 100%;">
<tbody>
<tr>
<td>
<font class="usernameatt">topman</font>
<br>
<font class="userlevelatt">Wonderkid</font>
</td>
</tr>
<tr>
<td>
<table class="ticketfftab ticketffsize">
<tbody>
<tr>
<td style="width: 60%; text-align:left;">
<button>FOLLOW</button>
<br>
<font class="pefwon">&bull;</font>
<font class="peflost">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="peflost">&bull;</font>
<font class="peflost">&bull;</font>
</td>
<td style="width: 40%; text-align:right;">
<font class="rightminititle">Status:</font>
<br>
<font class="ticketloststat">Lost</font>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>

<table cellpadding="5px" class="ticketoddtable ticketoddsize">
<tbody>
<tr>
<td style="width: 50%; text-align:left;">
<font class="rightminititle">Stake:</font>
<br>
<font class="odddata1">₦500</font>
</td>
<td style="width: 50%; text-align:right;">
<font class="rightminititle">Winnings:</font>
<br>
<font class="windata">₦64,175</font>
</td>
</tr>
<tr>
<td style="width: 50%; text-align:left;">
<font class="rightminititle">Odd:</font>
<br>
<font class="odddata2">128.35</font>
</td>
<td style="width: 50%; text-align:right;">
<font class="rightminititle">Games:</font>
<br>
<font class="odddata2">8</font>
</td>
</tr>
</tbody>
</table>
<table cellpadding="5px" class="ticketoddtable ticketoddsize2">
<tbody>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Everton - Huddersfield Town</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">12&amp;GG</font></td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Stoke City - Swansea</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">Over 2.5</font></td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Arsenal - Manchester Utd</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">2X</font></td>
</tr>

</tbody>
</table>
<table class="rebetbuttab rebetbuttsize">
<tbody>
<tr>
<td style="width: 60%; background-color:#BDBFC1;"><button-rebet-na>REBET TICKET</button-rebet-na></td>
<td style="width: 30%; text-align:center; background-color:#D2D3D5;">
<font class="rebetcount">&infin; 15</font>
</td>
<td style="width: 10%; background-color:#2C3E50;"><button-drop>&or;</button-rebet></td>
</tr>
</tbody>
</table>
</div>

<div class="c3 ticketcol1">
<table class="ticketopenhead ticketsize">
<tbody>
<tr>
<td style="width: 35%;"><img src="img/profilepic3.png" style="max-width:100%;height:auto;"></td>
<td style="width: 65%;">
<table style="max-width: 100%; height: 100%;">
<tbody>
<tr>
<td>
<font class="usernameatt">artfelt</font>
<br>
<font class="userlevelatt">Amateur</font>
</td>
</tr>
<tr>
<td>
<table class="ticketfftab ticketffsize">
<tbody>
<tr>
<td style="width: 60%; text-align:left;">
<button>FOLLOW</button>
<br>
<font class="peflost">&bull;</font>
<font class="peflost">&bull;</font>
<font class="peflost">&bull;</font>
<font class="peflost">&bull;</font>
<font class="peflost">&bull;</font>
</td>
<td style="width: 40%; text-align:right;">
<font class="rightminititle">Status:</font>
<br>
<font class="ticketopenstat">Open</font>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>

<table cellpadding="5px" class="ticketoddtable ticketoddsize">
<tbody>
<tr>
<td style="width: 50%; text-align:left;">
<font class="rightminititle">Stake:</font>
<br>
<font class="odddata1">₦500</font>
</td>
<td style="width: 50%; text-align:right;">
<font class="rightminititle">Winnings:</font>
<br>
<font class="windata">₦18,270</font>
</td>
</tr>
<tr>
<td style="width: 50%; text-align:left;">
<font class="rightminititle">Odd:</font>
<br>
<font class="odddata2">36.54</font>
</td>
<td style="width: 50%; text-align:right;">
<font class="rightminititle">Games:</font>
<br>
<font class="odddata2">5</font>
</td>
</tr>
</tbody>
</table>
<table cellpadding="5px" class="ticketoddtable ticketoddsize2">
<tbody>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Everton - Huddersfield Town</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">12&amp;GG</font></td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Stoke City - Swansea</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">Over 2.5</font></td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Arsenal - Manchester Utd</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">2X</font></td>
</tr>

</tbody>
</table>
<table class="rebetbuttab rebetbuttsize">
<tbody>
<tr>
<td style="width: 60%; background-color:#089B7F;"><button-rebet>REBET TICKET</button-rebet></td>
<td style="width: 30%; text-align:center; background-color:#D2D3D5;">
<font class="rebetcount">&infin; 105</font>
</td>
<td style="width: 10%; background-color:#2C3E50;"><button-drop>&or;</button-rebet></td>
</tr>
</tbody>
</table>
</div>

<div class="c3 ticketcol1">
<table class="ticketopenhead ticketsize">
<tbody>
<tr>
<td style="width: 35%;"><img src="img/profilepic2.png" style="max-width:100%;height:auto;"></td>
<td style="width: 65%;">
<table style="max-width: 100%; height: 100%;">
<tbody>
<tr>
<td>
<font class="usernameatt">flying_dutchman</font>
<br>
<font class="userlevelatt">Legendry</font>
</td>
</tr>
<tr>
<td>
<table class="ticketfftab ticketffsize">
<tbody>
<tr>
<td style="width: 60%; text-align:left;">
<button>FOLLOW</button>
<br>
<font class="peflost">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="pefwon">&bull;</font>
</td>
<td style="width: 40%; text-align:right;">
<font class="rightminititle">Status:</font>
<br>
<font class="ticketopenstat">Open</font>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>

<table cellpadding="5px" class="ticketoddtable ticketoddsize">
<tbody>
<tr>
<td style="width: 50%; text-align:left;">
<font class="rightminititle">Stake:</font>
<br>
<font class="odddata1">₦1,200</font>
</td>
<td style="width: 50%; text-align:right;">
<font class="rightminititle">Winnings:</font>
<br>
<font class="windata">₦14,616</font>
</td>
</tr>
<tr>
<td style="width: 50%; text-align:left;">
<font class="rightminititle">Odd:</font>
<br>
<font class="odddata2">12.18</font>
</td>
<td style="width: 50%; text-align:right;">
<font class="rightminititle">Games:</font>
<br>
<font class="odddata2">3</font>
</td>
</tr>
</tbody>
</table>
<table cellpadding="5px" class="ticketoddtable ticketoddsize2">
<tbody>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Everton - Huddersfield Town</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">12&amp;GG</font></td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Stoke City - Swansea</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">Over 2.5</font></td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Arsenal - Manchester Utd</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">2X</font></td>
</tr>

</tbody>
</table>
<table class="rebetbuttab rebetbuttsize">
<tbody>
<tr>
<td style="width: 60%; background-color:#089B7F;"><button-rebet>REBET TICKET</button-rebet></td>
<td style="width: 30%; text-align:center; background-color:#D2D3D5;">
<font class="rebetcount">&infin; 4205</font>
</td>
<td style="width: 10%; background-color:#2C3E50;"><button-drop>&or;</button-rebet></td>
</tr>
</tbody>
</table>
</div>
</div>

<!---------------------------------------------------DIVIDER----------------–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div class="row mgbt">

<div class="c3 ticketcol1">
<table class="ticketopenhead ticketsize">
<tbody>
<tr>
<td style="width: 35%;"><img src="img/profilepic7.png" style="max-width:100%;height:auto;"></td>
<td style="width: 65%;">
<table style="max-width: 100%; height: 100%;">
<tbody>
<tr>
<td>
<font class="usernameatt">naijaronaldo</font>
<br>
<font class="userlevelatt">1st Team Shirt</font>
</td>
</tr>
<tr>
<td>
<table class="ticketfftab ticketffsize">
<tbody>
<tr>
<td style="width: 60%; text-align:left;">
<button>FOLLOW</button>
<br>
<font class="peflost">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="peflost">&bull;</font>
<font class="peflost">&bull;</font>
</td>
<td style="width: 40%; text-align:right;">
<font class="rightminititle">Status:</font>
<br>
<font class="ticketopenstat">Open</font>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>

<table cellpadding="5px" class="ticketoddtable ticketoddsize">
<tbody>
<tr>
<td style="width: 50%; text-align:left;">
<font class="rightminititle">Stake:</font>
<br>
<font class="odddata1">₦100</font>
</td>
<td style="width: 50%; text-align:right;">
<font class="rightminititle">Winnings:</font>
<br>
<font class="windata">₦321,515</font>
</td>
</tr>
<tr>
<td style="width: 50%; text-align:left;">
<font class="rightminititle">Odd:</font>
<br>
<font class="odddata2">3125.15</font>
</td>
<td style="width: 50%; text-align:right;">
<font class="rightminititle">Games:</font>
<br>
<font class="odddata2">15</font>
</td>
</tr>
</tbody>
</table>
<table cellpadding="5px" class="ticketoddtable ticketoddsize2">
<tbody>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">West Brom - Crystal Palace</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">GG</font></td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Brighton - Liverpool</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">2</font></td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Leicester City - Burnley</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">12</font></td>
</tr>

</tbody>
</table>
<table class="rebetbuttab rebetbuttsize">
<tbody>
<tr>
<td style="width: 60%; background-color:#089B7F;"><button-rebet>REBET TICKET</button-rebet></td>
<td style="width: 30%; text-align:center; background-color:#D2D3D5;">
<font class="rebetcount">&infin; 98</font>
</td>
<td style="width: 10%; background-color:#2C3E50;"><button-drop>&or;</button-rebet></td>
</tr>
</tbody>
</table>
</div>

<div class="c3 ticketcol1">
<table class="ticketlosthead ticketsize">
<tbody>
<tr>
<td style="width: 35%;"><img src="img/profilepic8.png" style="max-width:100%;height:auto;"></td>
<td style="width: 65%;">
<table style="max-width: 100%; height: 100%;">
<tbody>
<tr>
<td>
<font class="usernameatt">lackotcha</font>
<br>
<font class="userlevelatt">Wonderkid</font>
</td>
</tr>
<tr>
<td>
<table class="ticketfftab ticketffsize">
<tbody>
<tr>
<td style="width: 60%; text-align:left;">
<button>FOLLOW</button>
<br>
<font class="pefwon">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="peflost">&bull;</font>
<font class="peflost">&bull;</font>
</td>
<td style="width: 40%; text-align:right;">
<font class="rightminititle">Status:</font>
<br>
<font class="ticketloststat">Lost</font>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>

<table cellpadding="5px" class="ticketoddtable ticketoddsize">
<tbody>
<tr>
<td style="width: 50%; text-align:left;">
<font class="rightminititle">Stake:</font>
<br>
<font class="odddata1">₦500</font>
</td>
<td style="width: 50%; text-align:right;">
<font class="rightminititle">Winnings:</font>
<br>
<font class="windata">₦64,175</font>
</td>
</tr>
<tr>
<td style="width: 50%; text-align:left;">
<font class="rightminititle">Odd:</font>
<br>
<font class="odddata2">128.35</font>
</td>
<td style="width: 50%; text-align:right;">
<font class="rightminititle">Games:</font>
<br>
<font class="odddata2">8</font>
</td>
</tr>
</tbody>
</table>
<table cellpadding="5px" class="ticketoddtable ticketoddsize2">
<tbody>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Everton - Huddersfield Town</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">12&amp;GG</font></td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Stoke City - Swansea</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">Over 2.5</font></td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Arsenal - Manchester Utd</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">2X</font></td>
</tr>

</tbody>
</table>
<table class="rebetbuttab rebetbuttsize">
<tbody>
<tr>
<td style="width: 60%; background-color:#BDBFC1;"><button-rebet-na>REBET TICKET</button-rebet-na></td>
<td style="width: 30%; text-align:center; background-color:#D2D3D5;">
<font class="rebetcount">&infin; 15</font>
</td>
<td style="width: 10%; background-color:#2C3E50;"><button-drop>&or;</button-rebet></td>
</tr>
</tbody>
</table>
</div>

<div class="c3 ticketcol1">
<table class="ticketopenhead ticketsize">
<tbody>
<tr>
<td style="width: 35%;"><img src="img/profilepic1.png" style="max-width:100%;height:auto;"></td>
<td style="width: 65%;">
<table style="max-width: 100%; height: 100%;">
<tbody>
<tr>
<td>
<font class="usernameatt">walewoo</font>
<br>
<font class="userlevelatt">Super-Sub</font>
</td>
</tr>
<tr>
<td>
<table class="ticketfftab ticketffsize">
<tbody>
<tr>
<td style="width: 60%; text-align:left;">
<button>FOLLOW</button>
<br>
<font class="peflost">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="peflost">&bull;</font>
<font class="pefwon">&bull;</font>
</td>
<td style="width: 40%; text-align:right;">
<font class="rightminititle">Status:</font>
<br>
<font class="ticketopenstat">Open</font>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>

<table cellpadding="5px" class="ticketoddtable ticketoddsize">
<tbody>
<tr>
<td style="width: 50%; text-align:left;">
<font class="rightminititle">Stake:</font>
<br>
<font class="odddata1">₦500</font>
</td>
<td style="width: 50%; text-align:right;">
<font class="rightminititle">Winnings:</font>
<br>
<font class="windata">₦18,270</font>
</td>
</tr>
<tr>
<td style="width: 50%; text-align:left;">
<font class="rightminititle">Odd:</font>
<br>
<font class="odddata2">36.54</font>
</td>
<td style="width: 50%; text-align:right;">
<font class="rightminititle">Games:</font>
<br>
<font class="odddata2">5</font>
</td>
</tr>
</tbody>
</table>
<table cellpadding="5px" class="ticketoddtable ticketoddsize2">
<tbody>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Everton - Huddersfield Town</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">12&amp;GG</font></td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Stoke City - Swansea</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">Over 2.5</font></td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Arsenal - Manchester Utd</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">2X</font></td>
</tr>

</tbody>
</table>
<table class="rebetbuttab rebetbuttsize">
<tbody>
<tr>
<td style="width: 60%; background-color:#089B7F;"><button-rebet>REBET TICKET</button-rebet></td>
<td style="width: 30%; text-align:center; background-color:#D2D3D5;">
<font class="rebetcount">&infin; 105</font>
</td>
<td style="width: 10%; background-color:#2C3E50;"><button-drop>&or;</button-rebet></td>
</tr>
</tbody>
</table>
<table style="width: 200px;">
<tbody>
<tr>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
</div>

<div class="c3 ticketcol1">
<table class="ticketwonhead ticketsize">
<tbody>
<tr>
<td style="width: 35%;"><img src="img/profilepic6.png" style="max-width:100%;height:auto;"></td>
<td style="width: 65%;">
<table style="max-width: 100%; height: 100%;">
<tbody>
<tr>
<td>
<font class="usernameatt">Skrillex19</font>
<br>
<font class="userlevelatt">Super Star</font>
</td>
</tr>
<tr>
<td>
<table class="ticketfftab ticketffsize">
<tbody>
<tr>
<td style="width: 60%; text-align:left;">
<button>FOLLOW</button>
<br>
<font class="peflost">&bull;</font>
<font class="peflost">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="peflost">&bull;</font>
<font class="pefwon">&bull;</font>
</td>
<td style="width: 40%; text-align:right;">
<font class="rightminititle">Status:</font>
<br>
<font class="ticketwonstat">Won</font>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>

<table cellpadding="5px" class="ticketoddtable ticketoddsize">
<tbody>
<tr>
<td style="width: 50%; text-align:left;">
<font class="rightminititle">Stake:</font>
<br>
<font class="odddata1">₦1,200</font>
</td>
<td style="width: 50%; text-align:right;">
<font class="rightminititle">Winnings:</font>
<br>
<font class="windata">₦14,616</font>
</td>
</tr>
<tr>
<td style="width: 50%; text-align:left;">
<font class="rightminititle">Odd:</font>
<br>
<font class="odddata2">12.18</font>
</td>
<td style="width: 50%; text-align:right;">
<font class="rightminititle">Games:</font>
<br>
<font class="odddata2">3</font>
</td>
</tr>
</tbody>
</table>
<table cellpadding="5px" class="ticketoddtable ticketoddsize2">
<tbody>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Everton - Huddersfield Town</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">12&amp;GG</font></td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Stoke City - Swansea</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">Over 2.5</font></td>
</tr>

<tr style="height: 10px;" class="gamedivider">
<td style="width: 170px; text-align:left;">
<font class="gamedata1">Arsenal - Manchester Utd</font>
</td>
<td style="width: 30px; text-align:right;">
<font class="gamedata2">2X</font></td>
</tr>

</tbody>
</table>
<table class="rebetbuttab rebetbuttsize">
<tbody>
<tr>
<td style="width: 60%; background-color:#BDBFC1;"><button-rebet-na>REBET TICKET</button-rebet-na></td>
<td style="width: 30%; text-align:center; background-color:#D2D3D5;">
<font class="rebetcount">&infin; 4205</font>
</td>
<td style="width: 10%; background-color:#2C3E50;"><button-drop>&or;</button-rebet></td>
</tr>
</tbody>
</table>
</div>
</div>

<!---------------------------------------------------DIVIDER----------------–––––––––––––––––––––––––––––––––––––––––––––––––– -->

<!-- Tickets Container END–––––––––––––––––––––––––––––––––––––––––––––––––– -->

</div>
<!-- Left Panel END–––––––––––––––––––––––––––––––––––––––––––––––––– -->

<!-- Right Panel 1 –––––––––––––––––––––––––––––––––––––––––––––––––– -->

<div class="c3 rightpanel">

<div class="row mgbt">
<div class="c11 shareabet end">
<!-- Sharebet Panel –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div class="row">
<div class="c12 heading1">SHARE YOUR TICKET</div>
</div>

<div class="row">
<div class="c12 sharebox">
<form style="text-align:left;">
  <p>Insert Bet Code:<br><input style="width:100%; text-transform:uppercase;" name="name" type="text" value="" /></p>
  <p>Add a Comment (Optional):<br><textarea cols="30" rows="2">Long text.</textarea><p/> 
</form>
<p align="center"><button-sharebet>SHAREBET</button-sharebet></p>


</div>
</div>
<!-- Sharebet Panel END–––––––––––––––––––––––––––––––––––––––––––––––––– -->

<!-- Recent Bet Panel –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div class="row">
<div class="c12 heading1">YOUR TICKETS</div>
</div>

<div class="row">
<div class="c12 recentbox">
You have no new ticket. Find a winning ticket and rebet.
</div>
</div>
<!-- Recent Bet Panel END––––––––––––––––––––––––––––––––––––––– -->
</div>
</div>

</div>

<!-- Right Panel 1 END–––––––––––––––––––––––––––––––––––––––––––––––––– -->


<!-- Right Panel 2 –––––––––––––––––––––––––––––––––––––––––––––––––– -->

<div class="c3 rightpanel">

<div class="row mgbt">
<div class="c11 topbettors end">
<!-- Top Bettors Panel –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div class="row">
<div class="c12 heading1">TOP BETTORS</div>
</div>

<div class="row">
<div class="c12 bettorsbox">
<table cellpadding="5px" class="topbettorstable ticketoddsize2">
<tbody>

<tr>
<table class="topbettorslist topbettorssize">
<tbody>
<tr>
<td style="width: 35%;"><img src="img/profilepic6.png" style="max-width:100%;height:auto;"></td>
<td style="width: 65%;">
<table style="max-width: 100%; height: 100%;">
<tbody>
<tr>
<td>
<font class="usernametb">skrilex19</font>
<br>
<font class="userleveltb">Superstar</font>
</td>
</tr>
<tr>
<td>
<table class="ticketfftab ticketffsize">
<tbody>
<tr>
<td style="width: 60%; text-align:left;"><br>
<font class="tbwinrate">RATE: 77%</font><br>
<font class="pefwon">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="pefwon">&bull;</font>
</td>
<td style="width: 40%; text-align:right;">
<button>FOLLOW</button>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</tr>

<tr>
<table class="topbettorslist topbettorssize">
<tbody>
<tr>
<td style="width: 35%;"><img src="img/profilepic2.png" style="max-width:100%;height:auto;"></td>
<td style="width: 65%;">
<table style="max-width: 100%; height: 100%;">
<tbody>
<tr>
<td>
<font class="usernametb">flying_dutchman</font>
<br>
<font class="userleveltb">Legendry</font>
</td>
</tr>
<tr>
<td>
<table class="ticketfftab ticketffsize">
<tbody>
<tr>
<td style="width: 60%; text-align:left;"><br>
<font class="tbwinrate">RATE: 50%</font><br>
<font class="peflost">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="peflost">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="pefwon">&bull;</font>
</td>
<td style="width: 40%; text-align:right;">
<button>FOLLOW</button>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</tr>

<tr>
<table class="topbettorslist topbettorssize">
<tbody>
<tr>
<td style="width: 35%;"><img src="img/profilepic7.png" style="max-width:100%;height:auto;"></td>
<td style="width: 65%;">
<table style="max-width: 100%; height: 100%;">
<tbody>
<tr>
<td>
<font class="usernametb">naijaronaldo 
</font>
<br>
<font class="userleveltb">1st Team Shirt</font>
</td>
</tr>
<tr>
<td>
<table class="ticketfftab ticketffsize">
<tbody>
<tr>
<td style="width: 60%; text-align:left;"><br>
<font class="tbwinrate">RATE: 63%</font><br>
<font class="peflost">&bull;</font>
<font class="peflost">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="pefwon">&bull;</font>
</td>
<td style="width: 40%; text-align:right;">
<button>FOLLOW</button>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</tr>

<tr>
<table class="topbettorslist topbettorssize">
<tbody>
<tr>
<td style="width: 35%;"><img src="img/profilepic8.png" style="max-width:100%;height:auto;"></td>
<td style="width: 65%;">
<table style="max-width: 100%; height: 100%;">
<tbody>
<tr>
<td>
<font class="usernametb">lackotcha </font>
<br>
<font class="userlevelatt">Wonderkid</font>
</td>
</tr>
<tr>
<td>
<table class="ticketfftab ticketffsize">
<tbody>
<tr>
<td style="width: 60%; text-align:left;"><br>
<font class="tbwinrate">RATE: 52%</font><br>
<font class="peflost">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="peflost">&bull;</font>
<font class="peflost">&bull;</font>
<font class="pefwon">&bull;</font>
</td>
<td style="width: 40%; text-align:right;">
<button>FOLLOW</button>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</tr>

<tr>
<table class="topbettorslist topbettorssize">
<tbody>
<tr>
<td style="width: 35%;"><img src="img/profilepic5.png" style="max-width:100%;height:auto;"></td>
<td style="width: 65%;">
<table style="max-width: 100%; height: 100%;">
<tbody>
<tr>
<td>
<font class="usernametb">thanos9 </font>
<br>
<font class="userlevelatt">Pro</font>
</td>
</tr>
<tr>
<td>
<table class="ticketfftab ticketffsize">
<tbody>
<tr>
<td style="width: 60%; text-align:left;"><br>
<font class="tbwinrate">RATE: 46%</font><br>
<font class="peflost">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="pefwon">&bull;</font>
<font class="peflost">&bull;</font>
<font class="pefwon">&bull;</font>
</td>
<td style="width: 40%; text-align:right;">
<button>FOLLOW</button>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</tr>

</tbody>
</table>
</div>
</div>
<!-- Top Bettors Panel END–––––––––––––––––––––––––––––––––––––––––––––––––– -->
</div>
</div>

</div>

<!-- Right Panel 2 END–––––––––––––––––––––––––––––––––––––––––––––––––– -->
</div>	

</div>
</div>

<!-- Full Page END–––––––––––––––––––––––––––––––––––––––––––––––––– -->

<!-- Footer –––––––––––––––––––––––––––––––––––––––––––––––––– -->

<div class="grid wfull footer">
<div class="grid w1080">

        <div class="row">
<div class="c12 footerin">
<p>© 2017 | Sharbet Inc. | All Rights Reserved</p>
</div>
	</div>	

        
</div>
</div>
	

<!-- Footer END –––––––––––––––––––––––––––––––––––––––––––––––––– ->


</body>
</html>