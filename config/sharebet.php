<?php
return [
    /**
     * API URL; with no trailing slash
     */
    'api_url' => env('API_URL', 'http://localhost')
];