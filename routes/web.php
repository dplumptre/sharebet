<?php

//Route::get('/', 'WelcomeController@index')->name('index');
//Route::view('/', 'welcome.welcome')->name('home');
Route::view('/', 'layouts.home')->name('home');
Route::redirect('/home', '/');

Route::get('profile',               'ProfileController@profile')->name('profile');
Route::get('profile/{username}',    'ProfileController@anotherUser')->name('another.user');
Route::get('share',                 'ShareBetController@index');

Route::get('tickets',               'ProfileController@tickets')->name('tickets');
Route::get('bettors',               'ProfileController@bettors')->name('bettors');
Route::get('most-rebets',           'ProfileController@mostRebets')->name('most.rebets');
Route::get('high-odds',             'ProfileController@highOdds')->name('high.odds');
Route::get('low-odds',              'ProfileController@lowOdds')->name('low.odds');

Route::group(['prefix' => 'welcome'], function () {
    Route::get('/',         'WelcomeController@index')->name('welcome');
    Route::get('profile',   'WelcomeController@profile');
    Route::post('profile',  'WelcomeController@post_profile')->name('post.profile');
});

Route::post('sharebet', 'ShareBetController@create');
Route::post('comments', 'CommentsController@create');

/*
 * Authentication
 * ~/start/username={username}
 */
Route::prefix('start')->group(function(){

    Route::get('/', 'StartController@index');
    Route::post('/', 'StartController@postRegister')->name('start.register');
});

Route::get('external',      'TestController@external');
Route::get('sample-games',  'TestController@sampleGames');
Route::name('post.games')->post('games', 'TestController@postSampleGames');

Route::get('logout', 'Auth\LoginController@logout');
Auth::routes();
