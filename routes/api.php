<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::prefix('v1')
    ->middleware('auth:api')
    ->group(function(){

    Route::get('/query-bet/{id}',           'API\ApiController@getQueryBet');
    Route::post('/query-bet',               'API\ApiController@postQueryBet');
    Route::post('/share-bet',               'API\ApiController@postShareBet');
    Route::get('/user/get-shared-betcodes', 'API\ApiController@getUserSharedBetCodes');
    Route::post('/rebet',                   'API\ApiController@rebet');
});