@extends('layouts.boot')

@section('title', 'Profile')
@section('meta')
    <meta name="turbolinks-visit-control" content="reload">
@endsection
@section('content')
    <div class="row" id="app">
        <div class="col-md-2 profile-column">
            <div class="pc bg-white">
                <div class="avatar">
                    <img src="/img/userpic.png" style="max-width:100%;height:auto;">
                </div>
                <div class="user-block">
                    <div class="username">{{$user->username}}</div>
                    <div class="user-title">{{$user->nickname}}</div>
                </div>
                <div class="action-section">
                    <div class="followers">
                        <div>{{$user->followers->count()}}</div>
                        <span class="ff">followers</span></div>
                    @if ( auth()->user()->id !== $user->id && !auth()->user()->isFollowing($user) )
                    <a href="#" class="button-follow">follow</a>
                    @endif
                    <div class="streak"></div>
                </div>
                <div class="badge-container">
                    <div class="badge">Badge</div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="row same-bg">
                <div class="col-md-4 shared-bet">
                    <div class="figure">{{$user->betcodes()->count()}}</div>
                    <div class="text">shared bet</div>
                </div>
                <div class="col-md-4 win-rate">
                    <div class="figure">67%</div>
                    <div class="text">win rate</div>
                </div>
                <div class="col-md-4 total-wins">
                    <div class="figure">₦52,700.00</div>
                    <div class="text">total winnings</div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 bet-head the_grey_color">bet history</div>
            </div>
            @foreach($data->chunk($chunk) as $bet_data)
                <div class="row profile-row" style="margin-top:15px;">
                    @foreach($bet_data as $bet)
                        <div class="col-md-{{12/$chunk}}">
                            <div class="ticketbox">
                                <div class="ticket-head open">
                                    <div class="th-left">
                                        <img src="/img/profilepic7.png">
                                    </div>
                                    <div class="th-right">
                                        <div class="top">
                                            <div class="username">{{$bet->user->username}}</div>
                                            <div class="user-level">{{$bet->user->nickname}}</div>
                                        </div>
                                        <div class="below">
                                            <div class="b-left">
                                                @if ( auth()->user()->id !== $user->id )
                                                    <a href="#" class="button" style="display:block;">follow</a>
                                                @endif
                                                <span class="peflost"></span>
                                                <span class="peflost"></span>
                                                <span class="pefwon"></span>
                                                <span class="pefwon"></span>
                                                <span class="pefwon"></span>
                                            </div>
                                            <div class="b-right">
                                                <div class="brief-h"
                                                     style="font-weight:500;color:#BDBFC1;font-size:11px">Status:
                                                </div>
                                                <div brief="" style="color:#2C3E50;font-weight:500;margin-top:-4px;">
                                                    Open
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="ticket-info">
                                    <div class="t-top">
                                        <div class="t-top-left">
                                            <span class="stake">Stake:</span>
                                            <span class="stake-amount">₦{{$bet->stake}}</span>
                                        </div>
                                        <div class="t-top-right">
                                            <span class="winnings">Winnings:</span>
                                            <span class="winnings-amount">₦{{number_format($bet->winnings, 2)}}</span>
                                        </div>
                                    </div>
                                    <div class="t-bottom">
                                        <div class="t-bottom-left">
                                            <span class="block odds">Odds:</span>
                                            <span class="block odds-amount">{{$bet->odds}}</span>
                                        </div>
                                        <div class="t-bottom-right">
                                            <span class="block games">Games:</span>
                                            <span class="block games-amount">{{$bet->games->count()}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="ticket-matches">
                                    @foreach($bet->games as $game)
                                        <div class="game-row">
                                            <div class="game">{{$game->match}}</div>
                                            <div class="game-prediction">{{$game->prediction}}</div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>
        <div class="col-md-3 share-bet-form">

        </div>
        <app-overlay text="loading..." v-if="modalHandler" @share="shareBetcode" @close="closeModal" :content="
        modalContent" :loading="loading" :alert="alert"></app-overlay>
    </div>
@endsection

@push('scripts')
@include('scripts.vue-betcodes-share')
@endpush