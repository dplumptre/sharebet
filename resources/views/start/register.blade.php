@extends('layouts.boot')
@section('page_title', 'Register New Account')
@section('scripts')
    <script>
        document.addEventListener("DOMContentLoaded", function(event){
            document.querySelector('input.disabled').addEventListener("focus", function(){
                this.blur();
            });
        });
    </script>
@endsection
@section('content')
   

    <div class="row" id="app">
        <div class="col-md-12">
            <div  share-bet-form">
            <div class="card">
                <div class="card-header" style='border-radius: 0;
                font-size: 12px;
                color: white;
                background-color: #2b3d4f;
                font-weight: bold;
                text-transform: uppercase;'>
                    SHARE YOUR TICKET
                </div>
                <div class="card-body" style="background: #dfdfdf">
                    <form action="{{route('start.register')}}" method="post">
                        {{csrf_field()}}
                        <div class="group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label>Username</label>
                            <input type="text" name="username" value="{{ $username ?:old('username') }}" class="form-control disabled">
                            @if ($errors->has('username'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('username') }}</strong>
                                </span>
                            @endif
                        </div>
                        <br>

                        <div class="group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label>Email</label>
                            <input type="text" name="email" value="{{old('email')}}" class="form-control" placeholder="Email Address" autofocus>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <br>

                        <div class="group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label>Phone <small><strong>(No leading +)</strong></small></label>
                            <input type="text" name="phone" class="form-control" value="{{old('phone')}}" placeholder="Phone Number">
                            @if ($errors->has('phone'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="group"><br>
                            <button type="submit"class="btn btn-success btn-block">Create my Account</button>
                        </div>
                    </form>
                </div>
            </div>



        </div>
        </div>
    </div>
@endsection
