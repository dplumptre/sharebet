<div class="c3 rightpanel">

    <div class="row mgbt">
        <div class="c11 topbettors end">
            <!-- Top Bettors Panel –––––––––––––––––––––––––––––––––––––––––––––––––– -->
            <div class="row">
                <div class="c12 heading1">TOP BETTORS</div>
            </div>

            <div class="row">
                <div class="c12 bettorsbox">
                    <table cellpadding="5px" class="topbettorstable ticketoddsize2">
                        <tbody>

                        <tr>
                            <table class="topbettorslist topbettorssize">
                                <tbody>
                                <tr>
                                    <td style="width: 35%;"><img src="img/profilepic6.png"
                                                                 style="max-width:100%;height:auto;"></td>
                                    <td style="width: 65%;">
                                        <table style="max-width: 100%; height: 100%;">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <font class="usernametb">skrilex19</font>
                                                    <br>
                                                    <font class="userleveltb">Superstar</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table class="ticketfftab ticketffsize">
                                                        <tbody>
                                                        <tr>
                                                            <td style="width: 60%; text-align:left;"><br>
                                                                <font class="tbwinrate">RATE: 77%</font><br>
                                                                <font class="pefwon">&bull;</font>
                                                                <font class="pefwon">&bull;</font>
                                                                <font class="pefwon">&bull;</font>
                                                                <font class="pefwon">&bull;</font>
                                                                <font class="pefwon">&bull;</font>
                                                            </td>
                                                            <td style="width: 40%; text-align:right;">
                                                                <button>FOLLOW</button>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </tr>

                        <tr>
                            <table class="topbettorslist topbettorssize">
                                <tbody>
                                <tr>
                                    <td style="width: 35%;"><img src="img/profilepic2.png"
                                                                 style="max-width:100%;height:auto;"></td>
                                    <td style="width: 65%;">
                                        <table style="max-width: 100%; height: 100%;">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <font class="usernametb">flying_dutchman</font>
                                                    <br>
                                                    <font class="userleveltb">Legendry</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table class="ticketfftab ticketffsize">
                                                        <tbody>
                                                        <tr>
                                                            <td style="width: 60%; text-align:left;"><br>
                                                                <font class="tbwinrate">RATE: 50%</font><br>
                                                                <font class="peflost">&bull;</font>
                                                                <font class="pefwon">&bull;</font>
                                                                <font class="peflost">&bull;</font>
                                                                <font class="pefwon">&bull;</font>
                                                                <font class="pefwon">&bull;</font>
                                                            </td>
                                                            <td style="width: 40%; text-align:right;">
                                                                <button>FOLLOW</button>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </tr>

                        <tr>
                            <table class="topbettorslist topbettorssize">
                                <tbody>
                                <tr>
                                    <td style="width: 35%;"><img src="img/profilepic7.png"
                                                                 style="max-width:100%;height:auto;"></td>
                                    <td style="width: 65%;">
                                        <table style="max-width: 100%; height: 100%;">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <font class="usernametb">naijaronaldo
                                                    </font>
                                                    <br>
                                                    <font class="userleveltb">1st Team Shirt</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table class="ticketfftab ticketffsize">
                                                        <tbody>
                                                        <tr>
                                                            <td style="width: 60%; text-align:left;"><br>
                                                                <font class="tbwinrate">RATE: 63%</font><br>
                                                                <font class="peflost">&bull;</font>
                                                                <font class="peflost">&bull;</font>
                                                                <font class="pefwon">&bull;</font>
                                                                <font class="pefwon">&bull;</font>
                                                                <font class="pefwon">&bull;</font>
                                                            </td>
                                                            <td style="width: 40%; text-align:right;">
                                                                <button>FOLLOW</button>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </tr>

                        <tr>
                            <table class="topbettorslist topbettorssize">
                                <tbody>
                                <tr>
                                    <td style="width: 35%;"><img src="img/profilepic8.png"
                                                                 style="max-width:100%;height:auto;"></td>
                                    <td style="width: 65%;">
                                        <table style="max-width: 100%; height: 100%;">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <font class="usernametb">lackotcha </font>
                                                    <br>
                                                    <font class="userlevelatt">Wonderkid</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table class="ticketfftab ticketffsize">
                                                        <tbody>
                                                        <tr>
                                                            <td style="width: 60%; text-align:left;"><br>
                                                                <font class="tbwinrate">RATE: 52%</font><br>
                                                                <font class="peflost">&bull;</font>
                                                                <font class="pefwon">&bull;</font>
                                                                <font class="peflost">&bull;</font>
                                                                <font class="peflost">&bull;</font>
                                                                <font class="pefwon">&bull;</font>
                                                            </td>
                                                            <td style="width: 40%; text-align:right;">
                                                                <button>FOLLOW</button>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </tr>

                        <tr>
                            <table class="topbettorslist topbettorssize">
                                <tbody>
                                <tr>
                                    <td style="width: 35%;"><img src="img/profilepic5.png"
                                                                 style="max-width:100%;height:auto;"></td>
                                    <td style="width: 65%;">
                                        <table style="max-width: 100%; height: 100%;">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <font class="usernametb">thanos9 </font>
                                                    <br>
                                                    <font class="userlevelatt">Pro</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table class="ticketfftab ticketffsize">
                                                        <tbody>
                                                        <tr>
                                                            <td style="width: 60%; text-align:left;"><br>
                                                                <font class="tbwinrate">RATE: 46%</font><br>
                                                                <font class="peflost">&bull;</font>
                                                                <font class="pefwon">&bull;</font>
                                                                <font class="pefwon">&bull;</font>
                                                                <font class="peflost">&bull;</font>
                                                                <font class="pefwon">&bull;</font>
                                                            </td>
                                                            <td style="width: 40%; text-align:right;">
                                                                <button>FOLLOW</button>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- Top Bettors Panel END–––––––––––––––––––––––––––––––––––––––––––––––––– -->
        </div>
    </div>

</div>

<!-- Right Panel 2 END–––––––––––––––––––––––––––––––––––––––––––––––––– -->