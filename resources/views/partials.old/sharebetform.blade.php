<div class="c3 rightpanel">
    <div class="row mgbt">
        <div class="c11 shareabet end">
            <!-- Sharebet Panel –––––––––––––––––––––––––––––––––––––––––––––––––– -->
            <div class="row">
                <div class="c12 heading1">SHARE YOUR TICKET</div>
            </div>

            <div class="row">
                <div class="c12 sharebox">
                    <form class="ta-l" method="post" action="/sharebet">
                        {{csrf_field()}}
                        <p>Insert Bet Code:
                            <br>
                            <input class="w-100" name="betcode" type="text" placeholder="Betcode" value="{{old('betcode')}}"/>
                            @if($errors->has('betcode'))
                                <span style="color: #ff574b;">{{$errors->first('betcode')}}</span>
                            @endif
                        </p>
                        <p>
                            Add a Comment (Optional):<br>
                            <textarea name="comment" cols="30" rows="2" placeholder="Comment"></textarea>
                        <p/>
                        <p align="center">
                            <button type="submit" class="button-sharebet">SHAREBET</button>
                        </p>
                    </form>
                </div>
            </div>
            <!-- Sharebet Panel END–––––––––––––––––––––––––––––––––––––––––––––––––– -->

            <!-- Recent Bet Panel –––––––––––––––––––––––––––––––––––––––––––––––––– -->
            <div class="row">
                <div class="c12 heading1">YOUR TICKETS</div>
            </div>

            <div class="row">
                <div class="c12 recentbox">
                    You have no new ticket. Find a winning ticket and rebet.
                </div>
            </div>
            <!-- Recent Bet Panel END––––––––––––––––––––––––––––––––––––––– -->
        </div>
    </div>

</div>
