@extends('layouts.app')
@push('scripts')
<script>
    $('#flash-overlay-modal').modal();
    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>
@endpush
@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                @include('flash::message')
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">Generate Betcodes</div>

                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger alert-important">
                                <ul class="list-unstyled">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="{{ route('post.games') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="count">Betcode Count</label>
                                <select name="count" id="count" class="form-control">
                                    <option value="">Choose One</option>
                                    <option value="5">5 Betcodes</option>
                                    <option value="10">10 Betcodes</option>
                                    <option value="15">15 Betcodes</option>
                                    <option value="20">20 Betcodes</option>
                                    <option value="30">30 Betcodes</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="truncate">Clear old data?</label>
                                <select name="truncate" id="truncate" class="form-control">
                                    <option value="">Choose one</option>
                                    <option value="true">Yes</option>
                                    <option value="false">No</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="expire_in">Expire In</label>
                                <select name="expire_in" id="expire_in" class="form-control">
                                    <option value="">Choose One</option>
                                    <option value="5">5 Minutes</option>
                                    <option value="10">10 Minutes</option>
                                    <option value="20">20 Minutes</option>
                                    <option value="30">30 Minutes</option>
                                    <option value="60">1 Hour</option>
                                    <option value="120">2 Hours</option>
                                    <option value="300">5 Hours</option>
                                    <option value="600">10 Hours</option>
                                    <option value="1200">20 Hours</option>
                                    <option value="1440">1 Day</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" name="submit" class="btn btn-primary">Generate</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                @if( $data->count() )
                    <table class="table">
                        <thead>
                        <tr>
                            <th style="width: 50%">Betcode</th>
                            <th>expire at</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $row)
                            <tr>
                                <td>{{$row->betcode}}</td>
                                <td>{{ \Carbon\Carbon::createFromTimestamp($row->expire_at, 'Africa/Lagos')->format('d-m-Y H:i:s') }}</td>
                                <?php
                                $diff = \Carbon\Carbon::now()->diffInSeconds(\Carbon\Carbon::createFromTimestamp($row->expire_at), false);
                                //$_d = \Carbon\Carbon::now()->diffForHumans(\Carbon\Carbon::createFromTimestamp($row->expire_at));
                                $_d = \Carbon\Carbon::createFromTimestamp($row->expire_at)->diffForHumans(now());
                                ?>
                                <td>
                                    @if( 0 > $diff )
                                        <span class="badge badge-danger">Expired</span>
                                    @else
                                        <span class="badge badge-success">{{$_d}}</span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{$data->links()}}
                @else
                    <div class="alert alert-warning">No Betcode records yet</div>
                @endif
            </div>
        </div>
    </div>
@endsection