<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    </ol>
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <div class="item active">
            <img src='{{ asset('/img/banner.png')}}' style="width:100%;"/>
            <div class="carousel-caption">
            </div>
        </div>
    </div>
</div>