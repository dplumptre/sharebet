@inject('request', 'Illuminate\Http\Request')
@auth
<nav class="navbar navbar-expand-lg navbar-light bg-white" id="main-nav">
    <div class="container">
        <!--  <a class="navbar-brand" href="#">Navbar</a> -->
        <button class="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="nav navbar-nav ml-auto navbar-right pull-right">

                <li class="nav-item">
                    <a class="nav-link" href="/">Home</a>
                </li>
                <li class="nav-item{{mark_url_active('tickets')}}">
                    <a class="nav-link"{{ !is_url('tickets') ? 'href='. route('tickets') : ''}}>Discover Betslips</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Bettors Leaderboard</a>
                </li>
                <li class="nav-item{{mark_url_active('profile')}}">
                    <a class="nav-link"{{ !is_url('profile') ? 'href='. route('profile') : ''}}>My Bets [{{auth()->user()->username}}]</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
@endauth