<nav class="navbar navbar-dark bg-dark navbar-expand-lg">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
          <a class="navbar-brand" href="#">SHARE BET</a>
          <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item active">
              <a class="nav-link" href="#">Links<span class="sr-only">(current)</span></a>
            </li>
          </ul>

          <span style='padding-right:20px;color:#089B7F;font-weight:bold'>

            @auth
            ( Welcome  {{auth()->user()->username}} ) balance: &#8358;{{ number_format(100000) }} 
           @endauth
          </span>
          @guest
          <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="text" placeholder="usernme" >
            <input class="form-control mr-sm-2" type="password" placeholder="password" >
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Login</button>
          </form>
          @endguest
        </div>
      </nav>