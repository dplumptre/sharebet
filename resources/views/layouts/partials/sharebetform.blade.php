<div class="card">
    <div class="card-header">
        SHARE YOUR TICKET
    </div>
    <div class="card-block">
        <form @keydown="clear($event.target.name)">
        <div class="form-group">
            <label for="usr">BetCode:</label>
            <input type="text" class="form-control" id="usr" v-model="betcode" name="betcode">
            <span class="help has-error" v-if="errors.betcode">@{{ errors.betcode[0] }}</span>
        </div>
        <div class="form-group">
            <label for="comment">Comment:</label>
            <textarea class="form-control" rows="2" id="comment" v-model="comment" name="comment"></textarea>
            <span class="help has-error" v-if="errors.comment">@{{ errors.comment[0] }}</span>
        </div>
        <div class="form-group">
            <p><a href="#" class="btn btn-success btn-block" @click.prevent="submitForm">SHARE BET</a></p>
        </div>
        </form>
    </div>
</div>