<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('meta')
    <meta name="turbolinks-visit-control" content="reload">
    <link rel="icon" type="img/png" href="/img/favicon.png">
    <title>@yield('title', 'Sharebet Central')</title>
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/style.css">
    <script src="/js/app.js" defer="defer"></script>
    @stack('scripts')
</head>
<body>
<main role="main" id="main">
    @include('layouts.partials.login_nav')
    @include('layouts.partials.bannerhead')
    @include('layouts.partials.midnav')
    <div class="container mt-5">
        @yield('content')
    </div>
</main>
@include('layouts.partials.footer')
</body>
</html>
