<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="en"> <![endif]-->
<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en"> <!--<![endif]-->
<head>

    <!-- Basic Page Needs –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>@yield('page_title', env('APP_NAME'))</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="icon" type="img/png" href="/img/favicon.png">
    <!-- Mobile Specific Metas –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <!-- CSS –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="stylesheet" href="/css/sharebet.css">
    <link rel="stylesheet" href="/css/style.css">
</head>

<body>
<!-- Header Content From BetSite –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div class="grid wfull betheader"></div>
<!-- Header Content From BetSite END–––––––––––––––––––––––––––––––––––––––––––––––--->

<!-- Main Banner –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div class="grid wfull mainbanner"></div>
<!-- Main Banner END–––––––––––––––––––––––––––––––––––––––––––––––--->

<!-- Global Menu –––––––––––––––––––––––––––––––––––––––––––––––––– -->
@include('layouts.partials.nav')
<!-- Global Menu END–––––––––––––––––––––––––––––––––––––––––––––––--->

<!-- Full Page –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div class="grid wfull page1 mgtop">
    {{--<div class="grid w1080">--}}
        <div class="row">
            @include('includes.flash')
            @yield('content')
        </div>
    {{--</div>--}}
</div>
<!-- Full Page END–––––––––––––––––––––––––––––––––––––––––––––––––– -->
@include('layouts.partials.footer')
<!-- Script
      –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<!--<script src="js/jquery.js"></script>-->
<!-- Include the Sidr JS -->
{{--<script src="/js/jquery-2.1.1.js"></script>--}}
<!--<script src="js/modernizr.js"></script>--> <!-- Modernizr -->
<script src="/js/app.js"></script>
@yield('scripts')
<script>
</script>
</body>
</html>