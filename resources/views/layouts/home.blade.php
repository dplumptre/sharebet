<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->

<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>ShareBet</title>
    <meta name="description" content="An innovative platform that exploits existing key betting cultures. Engaging both the offline & online betting community to directly share, bet and communicate">
    <meta name="author" content="Capital Activa LTD.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:200,400,300,900' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/css/frontend.css">
    <link rel="icon" type="img/png" href="/fe/img/favicon.png">
    <script src="/js/frontend.js"></script>
</head>
<body>

<div class="grid wfull headbanner">
    <div class="grid w1024">
        <div class="row">
            <div class="c12 headtop">
                <p><img src="/fe/img/logo.png" style="max-width:100%;height:auto;"></p>
            </div>
        </div>

        <div class="row">
            <div class="c12 head1">
                <h1>Innovating<br>Sports Betting</h1>
                <p class="f2">We enhance betting experiences for bettors and<br> form profitable partnerships with bookmakers.</p>
                <span class="f1">
                    <b>Launching Soon &nbsp;|&nbsp; hello@sharebet.com.ng</b><br><br><b>&or;</b>
                </span>
            </div>
        </div>
    </div>
</div>

<div class="grid wfull page1">
    <div class="grid w1024">
        <div class="row">
            <!--<div class="c12 top1">
            <p>we are also currently...</p>
            </div>--><br>&nbsp;<br>
        </div>

        <div class="row">
            <div class="c6 1l">&nbsp;
                <!--<span class="f2">ShareBet Bookmakers Platform [SBP]</span>
                <p><img src="img/1.png" style="max-width:100%;height:auto;"></p>

                <p class="f3">SBP is our very first product, built to integrate with existing bookmaker's platform. It is an innovative platform tailored for bettors to share and make their bet slips available for other bettors to re-bet or make informed decisions for their bets. SBP simplifies sports betting, making it more fun and social by reinventing existing cultures.</p>-->
            </div>
            <div class="c6 end 1r">
                <span class="f5">- <strong>Recent Project</strong></span>
                <p><span class="f4">An innovative platform that exploits existing key betting cultures. Engaging both the offline &amp;  online betting community to directly share, bet and communicate.</span></p>
                <!--<p><a href="intern.php"><img src="img/3.png" style="max-width:100%;height:auto;"></a></p>-->
                <p class="f3">ShareBet Bookmakers Platform <strong>SBP</strong> is our very first product, built to integrate with existing bookmaker's platform.<strong>SBP</strong> simplifies sports betting for bettors, making it more fun and social by reinventing existing betting cultures and also to help increase profit and customer base for bookmakers.</p>
                <p class="f3"><em>Currently engagening with top bookmakers in the Nigerian betting scene, SBP will soon be rolled out in the early stages of the 2018/19 sporting season.</em></p>
            </div>
        </div>
        <p>&nbsp;</p>
    </div>
</div>

<div class="grid wfull footer">
    <div class="grid w1024">
        <div class="row">
            <div class="c1 footerin">
                <p><img src="/fe/img/mini_sharebet_logo.png" style="max-width:100%;height:auto;"></p>
            </div>
            <div class="c4 footerinr">
                <p>Contact us for more enquiries;<br>
                    <strong>hello@sharebet.com.ng</strong></p>
            </div>
            <div class="c7 footerin">
                <p>Copyright © 2018 ShareBet by Capital Activa Limited </p>
            </div>
        </div>
    </div>
</div>
</body>
</html>