@extends('layouts.boot')
@section('title', 'Share a Bet')

@section('content')
    <div class="row" id="app">

        <div class="col-md-4 offset-md-4">
            <div class="share-bet-form">
                @include('layouts.partials.sharebetform')
            </div>
            <app-overlay text="loading..." v-if="modalHandler" @share="shareBetcode" @close="closeModal" :content="
        modalContent" :loading="loading" :alert="alert"></app-overlay>
        </div>
    </div>
@endsection

@push('scripts')
@include('scripts.vue-betcodes-share')
@endpush