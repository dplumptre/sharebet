<div class="modal fade" id="rebet-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">rebet ticket</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="d-flex flex-column" v-if="_.size(rebetDetails)">
                    <div class="p-2 layer-one">
                        <div class="odds-c">
                            <span class="odds-head">Odd:</span><br>
                            <span class="odds-value">@{{rebetDetails.odds}}</span>
                        </div>
                        <div class="games-c">
                            <span class="games-head">No. of Games</span><br>
                            <span class="games-value">@{{ rebetDetails.no_of_games }}</span>
                        </div>
                    </div>
                    <div class="p-2 layer-two">
                        <div class="row">
                            <label class="col-md-3 col-sm-12 col-12" for="amount">Stake:</label>
                            <div class="col-md-9 col-sm-12 col-12">
                                <input type="text" class="stake form-control" v-model="rebetDetails.stake" @keyup="setAmount(rebetDetails.stake)" style="display:inline-block">
                                <div class="currency" style="color:white">₦</div>
                                <div class="button-group amount">
                                    <span class="green-button 100" @click="setAmount(100)">100</span>
                                    <span class="green-button 200" @click="setAmount(200)">200</span>
                                    <span class="green-button 300" @click="setAmount(500)">500</span>
                                    <span class="green-button 400" @click="setAmount(1000)">1000</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="p-2 layer-three">
                        <div class="head">pot winnings</div>
                        <div style="font-size:22px;">@{{ rebetDetails.winnings | toNaira }}</div>
                    </div>
                </div>
                <div class="alert alert-warning" v-else v-text="rebetInfo">
                </div>
            </div>
            <div v-if="_.size(rebetDetails)">
                <div class="modal-footer" v-if="rebetDetails.status == 'open'">
                    {{--<button type="button" class="btn btn-primary send-bet">send bet</button>--}}
                    {{--<button type="button" class="btn btn-secondary cancel" data-dismiss="modal">cancel</button>--}}
                    <div class="send-bet" @click.prevent="submitRebet(rebetDetails.id, rebetDetails.stake)">send bet</div>
                    <div class="cancel" @click.prevent="closeRebetModal">Close</div>
                </div>
                <div class="modal-footer" v-else>
                    {{--<button type="button" class="btn btn-primary send-bet">send bet</button>--}}
                    {{--<button type="button" class="btn btn-secondary cancel" data-dismiss="modal">cancel</button>--}}
                    <div class="send-bet">bet expired</div>
                    <div class="cancel" @click.prevent="closeRebetModal">Close</div>
                </div>
            </div>
        </div>
    </div>
</div>