@if(notify()->ready())
@if(notify()->type() !== 'overlay')
<div class="alert alert-{{notify()->type()}}">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
    {{notify()->message()}}
</div>
@endif
@endif