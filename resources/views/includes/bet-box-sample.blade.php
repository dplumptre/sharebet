<div class="row profile-row">
    <div class="col-md-4">
        <div class="ticketbox">
            <div class="ticket-head open">
                <div class="th-left">
                    <img src="/img/profilepic7.png">
                </div>
                <div class="th-right">
                    <div class="top">
                        <div class="username">naijaronaldo</div>
                        <div class="user-level">1st team shirt</div>
                    </div>
                    <div class="below">
                        <div class="b-left">
                            <a href="#" class="button" style="display:block;">follow</a>
                            <span class="peflost"></span>
                            <span class="peflost"></span>
                            <span class="pefwon"></span>
                            <span class="pefwon"></span>
                            <span class="pefwon"></span>
                        </div>
                        <div class="b-right">
                            <div class="brief-h"
                                 style="font-weight:500;color:#BDBFC1;font-size:11px">Status:
                            </div>
                            <div brief="" style="color:#2C3E50;font-weight:500;margin-top:-4px;">
                                Open
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ticket-info">
                <div class="t-top">
                    <div class="t-top-left">
                        <span class="stake">Stake:</span>
                        <span class="stake-amount">₦100</span>
                    </div>
                    <div class="t-top-right">
                        <span class="winnings">Winnings:</span>
                        <span class="winnings-amount">₦321,515.65</span>
                    </div>
                </div>
                <div class="t-bottom">
                    <div class="t-bottom-left">
                        <span class="block odds">Odds:</span>
                        <span class="block odds-amount">312.54</span>
                    </div>
                    <div class="t-bottom-right">
                        <span class="block games">Games:</span>
                        <span class="block games-amount">10</span>
                    </div>
                </div>
            </div>
            <div class="ticket-matches">
                <div class="game-row">
                    <div class="game">Man.Utd - Liverpool</div>
                    <div class="game-prediction">GG</div>
                </div>
                <div class="game-row">
                    <div class="game">Man.Utd - Liverpool</div>
                    <div class="game-prediction">GG</div>
                </div>
                <div class="game-row">
                    <div class="game">Man.Utd - Liverpool</div>
                    <div class="game-prediction">GG</div>
                </div>
                <div class="game-row more" style="text-align:center;">
                    <a href="#game-collapse" data-toggle="collapse" style="text-decoration:none;">more
                        games</a>
                </div>
                <div class="collapse" id="game-collapse">
                    <div class="game-row">
                        <div class="game">Man.Utd - Liverpool</div>
                        <div class="game-prediction">GG</div>
                    </div>
                </div>
            </div>
            <div class="rebet-row">
                <div class="share-button">rebet ticket</div>
                <div class="rebet-times">98</div>
                <div class="r-collapse"><</div>
            </div>
        </div>
    </div>
</div>