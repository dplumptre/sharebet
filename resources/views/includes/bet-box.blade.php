@if ($data->count())
    @foreach($data->chunk($chunk) as $bet_data)
        <div class="row profile-row" style="margin-top:15px;">
            @foreach($bet_data as $bet)
                <div class="col-md-{{12/$chunk}}">
                    <div class="ticketbox">
                        <div class="ticket-head {{$bet->status}}">
                            <div class="th-left">
                                <img src="/img/profilepic7.png">
                            </div>
                            <div class="th-right">
                                <div class="top">
                                    <a href="{{route('another.user',['username'=>$bet->user->username])}}">
                                        <div class="username">{{$bet->user->username}}</div>
                                    </a>
                                    <div class="user-level">{{$bet->user->nickname}}</div>
                                </div>
                                <div class="below">
                                    <div class="b-left">
                                        @auth
                                        @if ( auth()->user()->id !== $bet->user->id && !auth()->user()->isFollowing($bet->user) )
                                            <a href="#" class="button" style="display:block;">follow</a>
                                        @endif
                                        @endauth
                                        <span class="peflost"></span>
                                        <span class="peflost"></span>
                                        <span class="pefwon"></span>
                                        <span class="pefwon"></span>
                                        <span class="pefwon"></span>
                                    </div>
                                    <div class="b-right">
                                        <div class="brief-h"
                                             style="font-weight:500;color:#BDBFC1;font-size:11px">Status:
                                        </div>
                                        <div brief="" style="color:#2C3E50;font-weight:500;margin-top:-4px;">
                                            {{$bet->status}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="ticket-info">
                            <div class="t-top">
                                <div class="t-top-left">
                                    <span class="stake">Stake:</span>
                                    <span class="stake-amount">₦{{$bet->stake}}</span>
                                </div>
                                <div class="t-top-right">
                                    <span class="winnings">Winnings:</span>
                                    <span class="winnings-amount">₦{{number_format($bet->winnings, 2)}}</span>
                                </div>
                            </div>
                            <div class="t-bottom">
                                <div class="t-bottom-left">
                                    <span class="block odds">Odds:</span>
                                    <span class="block odds-amount">{{$bet->odds}}</span>
                                </div>
                                <div class="t-bottom-right">
                                    <span class="block games">Games:</span>
                                    <span class="block games-amount">{{$bet->games->count()}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="ticket-matches">
                            @foreach($bet->games as $game)
                                <div class="game-row">
                                    <div class="game">{{$game->match}}</div>
                                    <div class="game-prediction">{{$game->prediction}}</div>
                                </div>
                            @endforeach
                        </div>
                        <div class="ticket-rebet">
                            <a href="#" class="{{$bet->status}}" @click.prevent="showRebetModal({{$bet->id}}, '{{$bet->status}}')">
                                <div class="rebet-link {{$bet->status}}">rebet ticket</div>
                            </a>
                            <div class="rebet-times">{{$bet->rebets->count()}}</div>
                            <div class="rebet-dropdown">
                                @svg('solid/angle-left', 'text-white')
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endforeach
@else
    <div class="alert alert-warning">You don't have any shared betcodes yet</div>
@endif
@include('includes.rebet-modal')