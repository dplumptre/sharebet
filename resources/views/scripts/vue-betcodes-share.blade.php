<script>
    document.addEventListener("turbolinks:load", (event) => {

        //do work
        @auth
            window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + '{{ auth()->user()->api_token }}';
                @endauth
        var el = document.getElementById('app');

        if (el != null) {

            Vue.filter('toNaira', function (value) {
                return `₦ ${value.toFixed(2)}`
            });

            let vm = new Vue({
                el: el,
                data: {
                    errors: {},
                    betcode: '',
                    comment: '',
                    modalHandler: false,
                    modalContent: {},
                    alert: {},
                    loading: false,
                    betcodeData: {},
                    rebetAmount: '',
                    rebetDetails: {},
                    rebetInfo: '',
                },
                methods: {
                    submitForm(){
                        let $this = this;
                        this.startLoading();
                        this.showModal();
                        axios.post('/api/v1/query-bet', {
                            betcode: $this.betcode,
                            comment: $this.comment,
                        })
                            .then(data => {
                                $this.stopLoading();
                                $this.modalContent = JSON.parse(data.data)
                            })
                            .catch(err => {
                                $this.closeModal();
                                $this.stopLoading();
                                $this.errors = err.response.data.errors;


                            });
                    },
                    shareBetcode(){
                        let $this = this;
                        this.modalContent = {};
                        this.startLoading();
                        axios.post('/api/v1/share-bet', {
                            betcode: $this.betcode,
                            comment: ''
                        }).then(data => {
                            $this.stopLoading();
                            $this.alert = {message: 'Betcode Shared', type: 'success'};
                            setTimeout(function () {
                                $this.closeModal();
                                $this.betcode = '';
                                $this.comment = '';
                                //window.location.assign('/welcome');
                                window.location.reload();
                            }, 5000);
                        }).catch(err => {
                            console.log(err)
                            $this.stopLoading();
                            $this.alert = {message: 'You supplied an Invalid Betcode', type: 'error'};
                        });
                    },
                    submitRebet($betcode_id, $stake){
                        this.rebetDetails = {};
                        this.rebetInfo = 'Rebeting Ticket...';
                        axios.post('/api/v1/rebet', {
                            betcode_id: $betcode_id,
                            stake: $stake
                        }).then(res => {
                            this.rebetInfo = 'Done...';
                            window.location.reload();
                        })
                            .catch(err => console.log(err.response.data));
                    },
                    showRebetModal(id, status){
                        let $this = this;
                        this.rebetInfo = 'Loading...';
                        if (status === "open") {
                            $('#rebet-modal').modal('show');
                            axios.get(`/api/v1/query-bet/${id}`)
                                .then(data => {
                                    console.log(data.data)
//                                    this.rebetDetails = data.data;
                                    this.rebetDetails = {
                                        'no_of_games': parseInt(data.data.no_of_games),
                                        'odds': parseFloat(data.data.odds),
                                        'status': data.data.status,
                                        'winnings': parseFloat(data.data.winnings),
                                        'id': data.data.id,
                                        'betcode': data.data.betcode,
                                        'stake': data.data.stake,
                                        'user_id': data.data.user_id,
                                    };
                                })
                                .catch(err => console.log(err));
                        } else {
                            return false;
                        }
                    },
                    setAmount(amount){
                        this.rebetDetails.stake = amount;
                        this.rebetDetails.winnings = this.rebetDetails.odds * amount;
                    },
                    showModal(){
                        this.modalHandler = true;
                    },
                    closeModal(){
                        this.modalHandler = false;
                        this.modalContent = {};
                        this.alert = {};
                    },
                    closeRebetModal(id){
                        $('#rebet-modal').modal('hide');
                        this.rebetDetails = {};
                    },
                    startLoading(){
                        this.loading = true;
                    },
                    stopLoading(){
                        this.loading = false;
                    },
                    clear(field){
                        if (field) {
                            delete this.errors[field];
                            return;
                        }
                        this.errors = {};
                    },
                    halt(){
                        return false;
                    },

                },
                mixins: [TurbolinksAdapter],
                mounted(){
                    $('#rebet-modal').modal({
                        backdrop: 'static',
                        keyboard: false,
                        show: false,
                    });
                }
            });
        }
    });
</script>