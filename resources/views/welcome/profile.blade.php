@extends('layouts.boot')

@section('title', 'Profile')
@section('meta')
@endsection
@section('content')
    <div class="row" id="app">
        <div class="col-md-2 profile-column">
            <div class="pc bg-white">
                <div class="avatar">
                    <img src="/img/userpic.png" style="max-width:100%;height:auto;">
                </div>
                <div class="user-block">
                    <div class="username">{{auth()->user()->username}}</div>
                    <div class="user-title">{{auth()->user()->nickname}}</div>
                </div>
                <div class="action-section">
                    <div class="followers">
                        <div>{{auth()->user()->followers->count()}}</div>
                        <span class="ff">followers</span></div>

                    {{--<a href="#" class="button-follow">follow</a>--}}

                    <div class="streak"></div>
                </div>
                <div class="badge-container">
                    <div class="badge">Badge</div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="row same-bg">
                <div class="col-md-4 shared-bet">
                    <div class="figure">{{auth()->user()->betcodes()->count()}}</div>
                    <div class="text">shared bet</div>
                </div>
                <div class="col-md-4 win-rate">
                    <div class="figure">{{auth()->user()->winRate()}}%</div>
                    <div class="text">win rate</div>
                </div>
                <div class="col-md-4 total-wins">
                    <div class="figure">₦{{number_format(auth()->user()->totalWinnings(), 2)}}</div>
                    <div class="text">total winnings</div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 bet-head the_grey_color">bet history</div>
            </div>
            @include('includes.bet-box', ['data' => $data])
        </div>
        <div class="col-md-3 share-bet-form">
            @include('layouts.partials.sharebetform')
        </div>
        <app-overlay text="loading..." v-if="modalHandler" @share="shareBetcode" @close="closeModal" :content="
        modalContent" :loading="loading" :alert="alert"></app-overlay>
    </div>
@endsection

@push('scripts')
@include('scripts.vue-betcodes-share')
@endpush