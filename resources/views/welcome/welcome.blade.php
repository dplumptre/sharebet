@extends('layouts.boot')

@section('title', 'Discover Tickets')

@section('content')
    <style type="text/css">
        #overlay {
            position: fixed; /* Sit on top of the page content */
            display: block; /* Hidden by default */
            width: 100%; /* Full width (cover the whole page) */
            height: 100%; /* Full height (cover the whole page) */
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(0, 0, 0, 0.5); /* Black background with opacity */
            z-index: 1002; /* Specify a stack order in case you're using a different order for other elements */
            /*cursor: pointer; !* Add a pointer on hover *!*/
        }

        #text {
            position: absolute;
            top: 5%;
            left: 50%;
            font-size: 20px;
            color: white;
            transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
        }

        #overlay a {
            color: white;
            font-weight: bold;
            zoom: 150%;
            rotation: 30deg;
            position: absolute;
            right: 10px;
            cursor: pointer;
            padding: 10px;
        }

        #overlay .content {
            margin-top: 40px;
            height: 400px;
        }

        .content ul {
            list-style: none;
            margin-top: 30px;
        }

        .content .list-group {
            margin-top: 0px;
        }

        .left {
            float: left;
        }

        .right {
            float: right;
        }

        .bg-dark {
            background: #2B3D4F !important;
        }

        .navbar-demmy ul li.active a {
            color: #089B7F !important;
            border: 1px solid #089B7F !important;

        }

        .navbar-demmy ul li a {
            font-family: 'Montserrat', sans-serif;
            color: #ffffff !important;
            text-transform: capitalize !important;
            font-size: 15px !important;
            padding-top: 10px !important;
            padding-right: 16px !important;
            padding-bottom: 10px !important;
            padding-left: 16px !important;

        }

        .navbar-demmy ul li a:hover {
            color: #089B7F !important;
        }

        .navbar-demmy ul li {

            padding-right: 7px !important;
        }

    </style>
    <div class="row" id="app">
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-12">
                    <nav class="row navbar navbar-expand-sm bg-dark navbar-demmy">

                        <ul class="navbar-nav">
                            <li class="nav-item{{mark_url_active('tickets')}}">
                                <a class="nav-link"{{ !is_url('tickets') ? 'href='. route('tickets') : ''}}>All Tickets</a>
                            </li>
                            <li class="nav-item">
                                <a class=" nav-link" href="#">Bettors You Follow</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Most Rebets</a>
                            </li>
                            <li class="nav-item{{mark_url_active('high.odds')}}">
                                <a class="nav-link"{{ !is_url('high.odds') ? 'href='. route('high.odds') : ''}}>High Odds</a>
                            </li>
                            <li class="nav-item{{mark_url_active('low.odds')}}">
                                <a class="nav-link"{{ !is_url('low.odds') ? 'href='. route('low.odds') : ''}}>Low Odds</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="col-md-12 bet-head the_grey_color">bet history</div>
            @include('includes.bet-box', ['data' => $data])
        </div>
        <div class="col-md-3 share-bet-form">
            @include('layouts.partials.sharebetform')
        </div>
        <app-overlay text="loading..." v-if="modalHandler" @share="shareBetcode" @close="closeModal" :content="
        modalContent" :loading="loading" :alert="alert"></app-overlay>
    </div>
@endsection

@push('scripts')
@include('scripts.vue-betcodes-share')
@endpush