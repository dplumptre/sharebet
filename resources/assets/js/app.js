
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


Vue.component('app-overlay', require('./components/AppOvelayComponent.vue'));

// Vue.component('example-component', require('./components/ExampleComponent.vue'));
//
// const app = new Vue({
//     el: '#app'
// });
function insertAfter(el, referenceNode) {
    referenceNode.parentNode.insertBefore(el, referenceNode.nextSibling);
}
function composeDiv(id = null) {
    var div_class = '#game-collapse';

    var div = document.createElement('div');
    var link = document.createElement('a');
    link.setAttribute('class', 'more-games');
    link.setAttribute('data-toggle', 'collapse');
    if (id !== null) {
        var div_class = div_class + '-' + id
    }
    link.setAttribute('href', div_class);
    var text = document.createTextNode('more games');
    link.appendChild(text);
    div.appendChild(link);
    div.setAttribute('class', 'game-row more');
    return div;
}
function setHeight() {
    var $main = document.getElementById("main");
    var intFrameHeight = window.innerHeight;

    if ($main != null){
        $main.style.minHeight = (intFrameHeight - 80) + 'px';
    }

    if (document.querySelector("#overlay .content")) {
        document.querySelector("#overlay .content").style.height = intFrameHeight + 'px';
    }
}

document.addEventListener("DOMContentLoaded", (event) => {

    setHeight();
    //get all ticket boxes
    var $ticketbox = document.querySelectorAll('.ticketbox');

    //iterate over .ticket-matches
    for (var $i = 0; $i < $ticketbox.length; $i++) {
        var $ticket = $ticketbox[$i].querySelector('.ticket-matches');

        //set ID for the collapsible
        var $collapse_id = $i;

        //apply DOM manipulation to boxes with more than 3 matches (children)
        if ($ticket.children.length > 3) {
            var $collapse_div = composeDiv($collapse_id);
            //wrapping collapse div
            var div = document.createElement('div');
            div.setAttribute('id', 'game-collapse-' + $collapse_id);
            div.setAttribute('class', 'collapse');

            //Get the parentNode and then the childNodes
            var nodes = $ticket.children[0].parentNode.childNodes;
            var $count = 0;
            //iterate over the childNodes
            for(var $j=0; $j<nodes.length; $j++)
            {
                //grab onlu Div nodes cos texts too exists
                if (nodes[$j].nodeName == "DIV"){
                    if ($count >= 3){
                        div.appendChild(nodes[$j]);
                    }
                    $count++;
                }
            }
            insertAfter($collapse_div, $ticket.lastElementChild);
            insertAfter(div, $collapse_div);
        }

    }
});
window.addEventListener("resize", setHeight, false);